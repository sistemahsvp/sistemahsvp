# README

A shot description and who is working on this repository

### Project Name

- Sistema para control y registro de medicamentos

### Team Members

* Jonnathan Rios Gomez
* Natalie Rojas Navarro
* Valeria Villalobos Gonzalez
* Miguel Oviedo Rojas

### Project Description

- The objective of this project is to develop a system for a hospital wich is needing
- a solution where they can have a more strict record of what meds are they delivering to patients
- and to trace the path that the meds pass from general warehouse to withdraw window where the patient get those meds

### How to install the project for development

* You will just need to configure the repository locally and clone it by using (git clone https://jonzxz@bitbucket.org/sistemahsvp/sistemahsvp.git dev)
* In order to run the project, you need to run the following command (npm init -y) in the terminal with the path: project-pharmacy-hsvp\backend
* And to run the backend run the following command (npm run dev) 

* In order to run the frontend, run in a new terminal the following command (ng serve -o) in the terminal with the path: project-pharmacy-hsvp
* Keep backend and frontend running at the same time

### Technology Used ###

* STACK MEAN
* MongoDB with MongoDB Atlas
* Express v4.17.2
* Angular v12.11.1
* NodeJs v14.15.1


### Download Links ###

* Express (https://www.npmjs.com/package/express)
* Angular (https://angular.io/cli)
* NodeJS (https://nodejs.org/en/blog/release/v14.15.1/)
* Angular Material (https://material.angular.io/guide/getting-started)
