const mongoose = require('mongoose');
const { Schema } = mongoose;

const RecetaSchema = new Schema({
    _id: {type: String, required: true }, //numero de receta
    cedula: {type: Number, required: true },
    nombre: {type: String, required: true },//nombre completo
    cod_medicamento: {type: String, required: true },
    nom_medicamento: {type: String, required: true },
    cod_indicacion: {type: Number, required: true },
    nom_indicacion: {type: String, required: true },
    estado: {type:String, require:true},
    cantidad: {type: Number, required: true },
    usuario: {type: String, required: true },
    fecha_entrada: {type: Date, required: true },
    fecha_retiro: {type: Date, required: false },
    fecha_reintegro: {type: Date, required: false},
    responsable: {type: String, required: true },
    mensaje: {type: String, required: false}
});

module.exports = mongoose.model('receta', RecetaSchema);