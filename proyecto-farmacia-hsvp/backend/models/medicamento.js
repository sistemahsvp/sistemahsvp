const mongoose = require('mongoose');
const { Schema } = mongoose;

const MedicamentoSchema = new Schema({
    _id: {type: String, required: true },
    nom_medicamento: {type: String, required: true }
});

module.exports = mongoose.model('medicamento', MedicamentoSchema);