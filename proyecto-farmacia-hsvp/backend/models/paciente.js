const mongoose = require('mongoose');
const { Schema } = mongoose;

const PacienteSchema = new Schema({
    _id: {type: Number, required: true },
    nombre: {type: String, required: true },
    apellido1: {type: String, required: true },
    apellido2: {type: String, required: true }
});

module.exports = mongoose.model('paciente', PacienteSchema);