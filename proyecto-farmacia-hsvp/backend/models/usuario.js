const mongoose = require('mongoose');
const { Schema } = mongoose;

const UsuarioSchema = new Schema({
    _id: { type: String, required: true },
    tipo: { type: String, required: true },
    nombre: { type: String, required: true },
    apellido1: { type: String, required: true },
    apellido2: { type: String, required: true },
    correo: { type: String, required: true },
    telefono: { type: Number, required: true },
    rol: { type: Number, required: true },
    clave: { type: String, required: true }
})

module.exports = mongoose.model('usuario', UsuarioSchema);
