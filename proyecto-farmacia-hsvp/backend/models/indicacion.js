const mongoose = require('mongoose');
const { Schema } = mongoose;

const IndicacionSchema = new Schema({
    _id: {type: Number, required: true },
    nom_indicacion: {type: String, required: true }
});

module.exports = mongoose.model('indicacione', IndicacionSchema);