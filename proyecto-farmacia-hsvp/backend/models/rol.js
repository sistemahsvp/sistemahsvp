const mongoose = require('mongoose');
const { Schema } = mongoose;

const RolSchema = new Schema({
    _id: {type: Number, required: true },
    nom_rol: {type: String, required: true }
});

module.exports = mongoose.model('role', RolSchema);