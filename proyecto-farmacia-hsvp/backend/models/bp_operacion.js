const mongoose = require('mongoose');
const { Schema } = mongoose;

const BPOperacionesSchema = new Schema({
    _id: {type: Number, required: true },
    nom_medicamento: {type: String, required: true },
    cantidad: {type: Number, required: true },
    fecha: {type: Date, required: true }
});

module.exports = mongoose.model('bp_operacione', BPOperacionesSchema);