const mongoose = require('mongoose');
const { Schema } = mongoose;

const BPMedicamentosSchema = new Schema({
    _id: {type: String, required: true },
    nom_medicamento: {type: String, required: true },
    cantidad: {type: Number, required: true }
});

module.exports = mongoose.model('bp_medicamento', BPMedicamentosSchema);