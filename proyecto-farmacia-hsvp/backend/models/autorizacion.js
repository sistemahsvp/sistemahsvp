const mongoose = require('mongoose');
const { Schema } = mongoose;

const AutorizacionSchema = new Schema({
    cedula: {type: Number, required: true },
    nombre: {type: String, required: true },//nombre completo
    cod_medicamento: {type: String, required: true },
    nom_medicamento: {type: String, required: true },
    dosis: {type: String, required: true },
    diagnostico: {type: String, required: true },
    clave_inicial: {type: String, required: true },
    clave_continuacion: {type: String, required: true },
    medico: {type: String, required: true },
    especialidad: {type: String, required: true },
    estado: {type: String, required: true },
    cantidad_retiros: {type: Number, required: true}
});

module.exports = mongoose.model('autorizacione', AutorizacionSchema);