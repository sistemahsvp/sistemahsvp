const mongoose = require('mongoose');
const { Schema } = mongoose;

const ReintegroSchema = new Schema({
    _id: {type: Number, required: true },
    num_receta: {type: String, required: true },
    usuario: {type: String, required: true },
    fecha_reintegro: {type: Date, required: true}
});

module.exports = mongoose.model('Reintegro', ReintegroSchema);