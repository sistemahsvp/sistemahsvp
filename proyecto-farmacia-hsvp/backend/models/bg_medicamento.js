const mongoose = require('mongoose');
const { Schema } = mongoose;

const BGMedicamentosSchema = new Schema({
    _id: {type: String, required: true },
    nom_medicamento: {type: String, required: true },
    cantidad: {type: Number, required: true }
});

module.exports = mongoose.model('bg_medicamento', BGMedicamentosSchema);