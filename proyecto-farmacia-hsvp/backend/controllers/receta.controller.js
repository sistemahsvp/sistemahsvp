const { notEqual, notStrictEqual } = require('assert');
const { find } = require('../models/receta');
const recetaModel = require('../models/receta');
const logger = require('../utils/logger');

const recetaController = {}

recetaController.getRecetas = async (req, res) => {
    try {
        const recetas = await recetaModel.find();
        res.json(recetas);
    } catch (error) {
        logger.error(`: al obtener todas las Recetas: ${error}`);
    }
}

recetaController.editReceta = async (req, res) => {
    try {
        //Fecha reintegro
        const fechaActualizada = new Date(req.body.fecha_reintegro);
        const fechaOriginalReintegro = new Date(fechaActualizada - (fechaActualizada.getTimezoneOffset() * 60000))
        req.body.fecha_reintegro = fechaOriginalReintegro
        //Fecha retiro
        const fechaActualizadaRetiro = new Date(req.body.fecha_retiro);
        const fechaOriginalRetiro = new Date(fechaActualizadaRetiro - (fechaActualizadaRetiro.getTimezoneOffset() * 60000))
        req.body.fecha_retiro = fechaOriginalRetiro
        await recetaModel.findByIdAndUpdate(req.params.id, req.body);
        res.json({
            status: 'Receta actualizada'
        })
    } catch (error) {
        logger.error(`: al actualizar una Receta: ${error}`);
    }
}

recetaController.getReceta = async (req, res) => {
    try {
        const receta = await recetaModel.findById({ _id: req.params.id });
        res.send(receta);
    } catch (error) {
        logger.error(`: al obtener una Receta: ${error}`);
    }
}

recetaController.createReceta = async (req, res) => {
    try {
        const receta = new recetaModel(req.body);
        //Fecha entrada
        const fechaOriginalEntrada = new Date(receta.fecha_entrada - (receta.fecha_entrada.getTimezoneOffset() * 60000))
        receta.fecha_entrada = fechaOriginalEntrada
        //Fecha retiro
        const fechaOriginalRetiro = new Date(receta.fecha_retiro - (receta.fecha_retiro.getTimezoneOffset() * 60000))
        receta.fecha_retiro = fechaOriginalRetiro
        //Fecha reintegro
        const fechaOriginalReintegro = new Date(receta.fecha_reintegro - (receta.fecha_reintegro.getTimezoneOffset() * 60000))
        receta.fecha_reintegro = fechaOriginalReintegro
        await receta.save()
        res.send({ message: 'Receta creada' })
    } catch (error) {
        logger.error(`: al crear una Receta: ${error}`);
    }
}

recetaController.eliminarReceta = async (req, res) => {
    try {
        await recetaModel.findOneAndRemove(req.params.id)
        res.json({
            status: 'Receta eliminado'
        })
    } catch (error) {
        logger.error(`: al eliminar una Receta: ${error}`);
    }
}

recetaController.getRecetasPorPaciente = async (req, res) => {
    try {
        const pacienteRecetas = await recetaModel.find({ cedula: req.params.identificacion })
        res.send(pacienteRecetas);
    } catch (error) {
        logger.error(`: al obtener las Recetas de un paciente: ${error}`);
    }

}

//traer receta con mensaje
recetaController.getRecetasMsj = async (req, res) => {
    try {
        const recetasMsj = await recetaModel.find({
            "mensaje": { "$exists": true },
            "$expr": { "$gt": [{ "$strLenCP": "$mensaje" }, 1] }
        })
        res.send(recetasMsj);
    } catch (error) {
        logger.error(`: al obtener las Recetas con un mensaje: ${error}`);
    }

}

recetaController.getReintegrosPorMes = async (req, res) => {
    try {
        const recetas = await recetaModel.find()

        var reintegrosPorMes = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

        recetas.forEach((element) => {
            if (element.estado = "Reintegrada") {
                const fecha_reintegro = new Date(element.fecha_reintegro)
                for (let i = 0; i <= 11; i++) {
                    if (fecha_reintegro.getUTCMonth() == i) {
                        reintegrosPorMes[i] += 1;
                    }

                }
            }
        })
        res.send(reintegrosPorMes)
    } catch (error) {
        logger.error(`: al obtener los Reintegros por mes: ${error}`);
    }
}

//Recetas por cedula y cod medicamento
recetaController.getRecetasPorCedulaMedicamento = async (req, res) => {
    try {
        const recetasCedulaMedicamento = await recetaModel.find({ cedula: req.params.identificacion, cod_medicamento: req.params.cod_medicamento });
        res.send(recetasCedulaMedicamento);
    } catch (error) {
        logger.error(`: al obtener las Recetas por cédula y código de medicamento: ${error}`);
    }
}

module.exports = recetaController