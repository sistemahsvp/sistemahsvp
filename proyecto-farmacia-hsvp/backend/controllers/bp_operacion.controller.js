const bpOperacionController = {}
const bpOperacionModel = require('../models/bg_operacion');
const logger = require('../utils/logger');

bpOperacionController.getOperaciones = async(req, res) => {
    try {
        const operacion = await bpOperacionModel.find();
        res.json(operacion);    
    } catch (error) {
        logger.error(`: al traer operaciones en Bodega: ${error}`);
    }
}

bpOperacionController.editOperacion = async(req, res) => {
    try {
        await bpOperacionModel.findByIdAndUpdate(req.params.id, req.body);
        res.json({
            status: 'Operacion actualizada'
        })    
    } catch (error) {
        logger.error(`: al editar operación en Bodega - ${req.params.id} : ${error}`);
    }
}

bpOperacionController.getOperacion = async(req, res) => {
    try {
        const reintegro = await bpOperacionModel.findById({_id: req.params.id});
        res.send(reintegro);    
    } catch (error) {
        logger.error(`: al traer operación en Bodega - ${req.params.id} : ${error}`);
    }
}

bpOperacionController.createOperacion = async(req, res) => {
    try {
        const reintegro = new bpOperacionModel(req.body);
        await reintegro.save()
        res.send({message: 'Operacion creada'})    
    } catch (error) {
        logger.error(`: al crear operación en Bodega - ${req.params.id} : ${error}`);
    }
}

bpOperacionController.eliminarOperacion = async(req, res) => {
    try {
        await bpOperacionModel.findOneAndRemove(req.params.id)
        res.json({
            status: 'Operacion eliminada'
        })    
    } catch (error) {
        logger.error(`: al eliminar operación en Bodega - ${req.params.id} : ${error}`);
    }
}

module.exports = bpOperacionController