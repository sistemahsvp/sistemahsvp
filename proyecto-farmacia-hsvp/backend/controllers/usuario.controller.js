const Usuario = require('../models/usuario.js');
const bcrypt = require('bcrypt');
const logger = require('../utils/logger.js');
const salt = 10;

const usuarioController = {};

usuarioController.getUsuario = async (req, res) => {
    try {
        const usuario = await Usuario.findById(req.params.id);
        if (usuario != null) {
            res.json(usuario);
        } else {
            res.json({
                estado: 'Usuario no encontrado!'
            })
        }
    } catch (error) {
        logger.error(`: al obtener un Usuario: ${error}`);
    }
}

usuarioController.getUsuarioLogin = async (req, res) => {
    try {
        const usuario = await Usuario.findById(req.params.id);
        if (usuario != null) {

            bcrypt.compare(req.params.clave, usuario.clave, async (err, same) => {
                if (usuario != null && same === true) {
                    usuario.clave = req.params.clave;
                    res.json(usuario);
                    logger.info(`Usuario: ${usuario._id} inició sesión`);
                } else {
                    res.json({
                        estado: 'Usuario no encontrado!'
                    })
                }
            })
        } else {
            res.json({
                estado: 'Usuario no encontrado!'
            })
        }
    } catch (error) {
        logger.error(`: al obtener un Usuario para el inicio de sesión: ${error}`);
    }
}

usuarioController.getUsuarios = async (req, res) => {
    try {
        const usuarios = await Usuario.find();
        res.json(usuarios);
    } catch (error) {
        logger.error(`:al obtener todos los Usuarios: ${error}`);
    }
}

usuarioController.crearUsuario = async (req, res) => {
    try {
        const usuario = new Usuario(req.body);
        bcrypt.hash(req.body.clave, salt, async (err, res) => {
            usuario.clave = res;
            await usuario.save();
        });
        res.json({
            'status': 'Usuario guardado'
        })
    } catch (error) {
        logger.error(`: al crear un Usuario: ${error}`);
    }

}

usuarioController.actualizarUsuario = async (req, res) => {
    try {
        const { id } = req.params;
        const usuarioBD = await Usuario.findById(id);
        const usuario = {
            _id: req.body._id,
            tipo: req.body.tipo,
            nombre: req.body.nombre,
            apellido1: req.body.apellido1,
            apellido2: req.body.apellido2,
            correo: req.body.correo,
            telefono: req.body.telefono,
            rol: req.body.rol,
            clave: req.body.clave
        }


        if (usuarioBD.clave === usuario.clave) {
            await Usuario.findByIdAndUpdate(id, { $set: usuario }, { $new: true });
        } else {
            bcrypt.hash(usuario.clave, salt, async (err, hash) => {
                usuario.clave = hash;
                await Usuario.findByIdAndUpdate(id, { $set: usuario }, { $new: true });
            })
        }
        res.json({
            'status': 'Usuario actualizado'
        })
    } catch (error) {
        logger.error(`: al actualizar un Usuario: ${error}`);
    }
}

usuarioController.eliminarUsuario = async (req, res) => {
    try {
        await Usuario.findByIdAndRemove(req.params.id);
        res.json({
            'status': 'Usuario eliminado!'
        })
    } catch (error) {
        logger.error(`: al eliminar un Usuario: ${error}`);
    }
}

module.exports = usuarioController;