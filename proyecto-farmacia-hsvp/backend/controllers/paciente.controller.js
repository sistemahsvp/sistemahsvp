const pacienteModel = require('../models/paciente');
const logger = require('../utils/logger');

const pacienteController = {}

pacienteController.getPacientes = async (req, res) => {
    try {
        const pacientes = await pacienteModel.find();
        res.json(pacientes);
    } catch (error) {
        logger.error(`: al obtener todos los Pacientes: ${error}`);
    }

}

pacienteController.editPaciente = async (req, res) => {
    try {
        await pacienteModel.findByIdAndUpdate(req.params.id, req.body);
        res.json({
            status: 'Paciente actualizado'
        })
    } catch (error) {
        logger.error(`: al actualizar un Paciente: ${error}`);
    }
}

pacienteController.getPaciente = async (req, res) => {
    try {
        const paciente = await pacienteModel.findById({ _id: req.params.id });
        res.send(paciente);
    } catch (error) {
        logger.error(`: al obtener un Paciente: ${error}`);
    }
}

pacienteController.createPaciente = async (req, res) => {
    try {
        const paciente = new pacienteModel(req.body);
        await paciente.save()
        res.send({ message: 'Paciente creado' });
    } catch (error) {
        logger.error(`: al crear un Paciente: ${error}`);
    }

}

pacienteController.eliminarPaciente = async (req, res) => {
    try {
        await pacienteModel.findOneAndRemove(req.params.id)
        res.json({
            status: 'Paciente eliminado'
        });
    } catch (error) {
        logger.error(`: al eliminar un Paciente: ${error}`);
    }
}

module.exports = pacienteController