const rolModel = require('../models/rol.js');
const logger = require('../utils/logger.js');

const rolController = {}

rolController.getRoles = async (req, res) => {
    try {
        const roles = await rolModel.find();
        res.json(roles);
    } catch (error) {
        logger.error(`: al obtener todos los Roles: ${error}`);
    }
}

rolController.actualizarRol = async (req, res) => {
    try {
        await rolModel.findByIdAndUpdate(req.params.id, req.body);
        res.json({
            status: 'Rol actualizado'
        })
    } catch (error) {
        logger.error(`: al actualizar un Rol: ${error}`);
    }
}

rolController.getRol = async (req, res) => {
    try {
        const rol = await rolModel.findById(req.params.id);
        res.send(rol);
    } catch (error) {
        logger.error(`: al obtener un Rol: ${error}`);
    }
}

rolController.crearRol = async (req, res) => {
    try {
        const rol = new rolModel(req.body);
        await rol.save()
        res.send({ message: 'Rol creado' });
    } catch (error) {
        logger.error(`: al crear un Rol: ${error}`);
    }
}

rolController.eliminarRol = async (req, res) => {
    try {
        await rolModel.findOneAndRemove(req.params.id)
        res.json({
            status: 'Rol eliminado'
        })
    } catch (error) {
        logger.error(`: al eliminar un Rol: ${error}`);
    }
}

module.exports = rolController