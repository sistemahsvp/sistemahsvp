const Medicamento = require('../models/medicamento');
const logger = require('../utils/logger');

const medicamentoCtrl = {}

medicamentoCtrl.createMedicamentos = async (req, res) => {
    try {
        const nuevoMedicamento = new Medicamento(req.body);
        await nuevoMedicamento.save();
        res.send({ msg: 'Medicamento creado' });
    } catch (error) {
        logger.error(`: al crear un Medicamento: ${error}`);
    }
}

medicamentoCtrl.getMedicamentos = async (req, res) => {
    try {
        const medicamentos = await Medicamento.find();
        res.json(medicamentos);
    } catch (error) {
        logger.error(`: al obtener todos los Medicamentos: ${error}`);
    }
}

medicamentoCtrl.getMedicamento = async (req, res) => {
    try {
        const medicamento = await Medicamento.findById(req.params.id);
        res.send(medicamento);
    } catch (error) {
        logger.error(`: al obtener un Medicamento: ${error}`);
    }
}

medicamentoCtrl.updateMedicamentos = async (req, res) => {
    try {
        await Medicamento.findByIdAndUpdate(req.params.id, req.body);
        res.json({ msg: 'Medicamento actualizado' });
    } catch (error) {
        logger.error(`: al actualizar un Medicamento: ${error}`);
    }
}

medicamentoCtrl.deleteMedicamentos = async (req, res) => {
    try {
        await Medicamento.findByIdAndDelete(req.params.id);
        res.json({ msg: 'Medicamento eliminado' });
    } catch (error) {
        logger.error(`: al eliminar un Medicamento: ${error}`);
    }
}

module.exports = { medicamentoCtrl }