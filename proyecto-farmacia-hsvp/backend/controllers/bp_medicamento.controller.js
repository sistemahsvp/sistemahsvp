const bpMedicamentoCtrl = {}
const { send } = require('process')
const medicamentos_bp = require('../models/bp_medicamento')
const medicamento = require('../models/medicamento.js')
const logger = require('../utils/logger');

//creamos las funciones de CRUD
bpMedicamentoCtrl.getMedicamentos = async (req, res) => {
    try {
        const bpMedicamentos = await medicamentos_bp.find() 
        res.json(bpMedicamentos)    
    } catch (error) {
        logger.error(`: traer medicamentos en Bodega : ${error}`);
    }
}

bpMedicamentoCtrl.editMedicamento = async (req, res) => {
    try {
        await medicamentos_bp.findByIdAndUpdate(req.params.id, req.body)
        res.json({status: 'Cantidad Actualizada'})    
    } catch (error) {
        logger.error(`: editar cantidad de medicamento en Bodega - ${req.params.id} : ${error}`);
    }
}

bpMedicamentoCtrl.getMedicamento = async (req, res) => {
    try {
        const Medicamento = await medicamentos_bp.findById({_id: req.params.id})
        res.send(Medicamento)    
    } catch (error) {
        logger.error(`: buscar medicamento en Bodega - ${req.params.id} : ${error}`);
    }
}


bpMedicamentoCtrl.createMedicamento = async (req, res) => {
    try {
        const newMedicamentos_bp = new medicamentos_bp(req.body)
        await newMedicamentos_bp.save()
        res.send({message: 'Medicamento Creado'})    
    } catch (error) {
        logger.error(`: crear medicamento en Bodega - ${req.params.id} : ${error}`);
    }
}

module.exports = bpMedicamentoCtrl;