const autorizacionController = {}

const autorizacionModel = require('../models/autorizacion');
const logger = require('../utils/logger');

autorizacionController.getAutorizaciones = async(req, res) => {
    try {
        const autorizaciones = await autorizacionModel.find();
        res.json(autorizaciones);
    } catch (error) {
        logger.error(`: al traer todas las Autorizaciones : ${error}`);
    }
}

autorizacionController.editAutorizacion = async(req, res) => {
    try {
        await autorizacionModel.findByIdAndUpdate(req.params.id, req.body);
        res.json({status: 'Autorizacion actualizada'});
    } catch (error) {
        logger.error(`: al Editar una Autorización: ${req.params.id} : ${error}`);
    }
}

autorizacionController.getAutorizacion = async(req, res) => {
    try {
       const autorizacion = await autorizacionModel.findById({_id: req.params.id});
        res.send(autorizacion); 
    } catch (error) {
        logger.error(`: al traer Autorización ${req.params.id} : ${error}`);
    }
}

autorizacionController.createAutorizacion = async(req, res) => {
    try {
        const autorizacion = new autorizacionModel(req.body);
        await autorizacion.save()
        res.send({message: 'Autorzacion creada'});
    } catch (error) {
        logger.error(`: al crear Autorización nueva : ${error}`);
    }
}

autorizacionController.eliminarAutorizacion = async(req, res) => {
    try {
        await autorizacionModel.findByIdAndRemove(req.params.id)
        res.json({'status': 'Autorizacion eliminada'});
    } catch (error) {
        logger.error(`: al eliminar Autorización ${req.params.id} : ${error}`);
    }
}


//Ruta para Autorizacion segun la Clave de Inicio
autorizacionController.getAutorizacionClaveInicio = async(req, res) => {
    try {
        const autorizacionClaveInicio = 
            await autorizacionModel.findOne({clave_inicial: req.params.clave_inicial});
        res.send(autorizacionClaveInicio);
    } catch (error) {
        logger.error(`: al traer Autorización por Clave de Inicio - ${req.params.id} : ${error}`);
    }
}


    
autorizacionController.buscarAutorizacionCedMed = async(req, res) => {
    try {
        const autorizacion = await autorizacionModel.find({cedula: req.params.cedula,
            cod_medicamento: req.params.codMed});
        res.send(autorizacion);
    } catch (error) {
        logger.error(`: al consultar Autorización del Paciente - ${req.params.cedula} : ${error}`);
    }
}

autorizacionController.actualizarPorClaveInicial = async(req, res) => {
    try {
        const autorizacionClaveInicio = 
            await autorizacionModel.findOneAndUpdate({clave_inicial: req.params.clave_inicial}, req.body)
        res.send(autorizacionClaveInicio)    
    } catch (error) {
        logger.error(`: al actualizar Autorización del Paciente - ${req.params.cedula} : ${error}`);
    }
}

module.exports = autorizacionController