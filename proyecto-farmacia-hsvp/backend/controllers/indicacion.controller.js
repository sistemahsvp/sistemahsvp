const indicacionModel = require('../models/indicacion');
const logger = require('../utils/logger');

const indicacionController = {}

indicacionController.getIndicaciones = async (req, res) => {
    try {
        const indicacion = await indicacionModel.find();
        res.json(indicacion);
    } catch (error) {
        logger.error(`: al obtener todas las Indicaciones: ${error}`);
    }
}

indicacionController.editIndicacion = async (req, res) => {
    try {
        await indicacionModel.findByIdAndUpdate(req.params.id, req.body);
        res.json({
            status: 'Indicacion actualizada'
        });
    } catch (error) {
        logger.error(`: al actualizar una Indicación: ${error}`);
    }
}

indicacionController.getIndicacion = async (req, res) => {
    try {
        const indicacion = await indicacionModel.findById({ _id: req.params.id });
        res.send(indicacion);
    } catch (error) {
        logger.error(`: al obtener una Indicación: ${error}`);
    }
}

indicacionController.createIndicacion = async (req, res) => {
    try {
        const indicacion = new indicacionModel(req.body);
        await indicacion.save();
        res.send({ message: 'Indicacion creada' });
    } catch (error) {
        logger.error(`: al crear una Indicación: ${error}`);
    }

}

indicacionController.eliminarIndicacion = async (req, res) => {
    try {
        await indicacionModel.findOneAndRemove(req.params.id)
        res.json({
            status: 'Indicacion eliminada'
        });
    } catch (error) {
        logger.error(`: al eliminar una Indicación: ${error}`);
    }
}

module.exports = indicacionController