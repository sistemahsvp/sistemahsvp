const { restart } = require('nodemon')
const bg_medicamento = require('../models/bg_medicamento')
const logger = require('../utils/logger');

const bgController = {}

bgController.getbg_medicamentos = async(req, res) => {
    try {
        const medicamentosBG = await bg_medicamento.find();
        res.json( medicamentosBG);
    } catch (error) {
        logger.error(`: al traer Medicamentos de Bodega : ${error}`);
    }
}

bgController.getbg_medicamento = async(req, res) => {
    try {
        const medicamentoBG = await bg_medicamento.findById(req.params.id);
        res.send(medicamentoBG);
    } catch (error) {
        logger.error(`: al buscar el Medicamento de Bodega ${req.params.id} : ${error}`);
    }
}

bgController.createMedicamento = async (req, res) => {
    try {
        const medicamentoBG = new bg_medicamento(req.body)
        await medicamentoBG.save()
        res.send({message: 'Medicamento Creado'});
    } catch (error) {
        logger.error(`: al crear Medicamento en Bodega ${req.params.id} : ${error}`);
    }
}

bgController.updatebg_medicamento = async(req, res) => {
    try {
        await bg_medicamento.findByIdAndUpdate(req.params.id, req.body);
        res.json({msg: 'Cantidad actualizada'});
    } catch (error) {
        logger.error(`: al actualizar Medicamento en Bodega ${req.params.id} : ${error}`);
    }
}

bgController.createbg_medicamento = async(req, res) =>{
    try {
        const medicamento = new bg_medicamento(req.body);
        medicamento.save()
        res.json({
            'status': 'Cantidad ingresada !!'
        });
    } catch (error) {
        logger.error(`: al crear Medicamento en Bodega ${req.params.id} : ${error}`);
    }
}

bgController.getLastNMedicamentos = async(req, res) =>{
    try {
        const listaMedicamentos = await bg_medicamento.find({cantidad: {$lt: req.params.number}}).sort({cantidad: -1})
        res.json(listaMedicamentos);
    } catch (error) {
        logger.error(`: al traer el último Medicamento en Bodega ${req.params.id} : ${error}`);
    }
}


module.exports = bgController;