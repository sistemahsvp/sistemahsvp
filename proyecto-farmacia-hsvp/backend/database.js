const mongoose = require('mongoose');
//HSVP Pass1234
//const URI = 'mongodb+srv://HSVP:Pass1234@hsvp.qudt7.mongodb.net/HSVPBD?retryWrites=true&w=majority';

const URI = process.env.URI_KEY;

mongoose.connect(URI)
    .then(db => console.log('Database connected'))
    .catch(err => console.error(err))

module.exports = mongoose;