require('dotenv').config()


const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const logger = require('./utils/logger');

const app = express();

const { mongoose } = require('./database.js');

//Settings
app.set('port', process.env.PORT || 3000)

//Middlewares
//app.use(cors()) //quitar a la hora del push
app.use(morgan('dev'))
app.use(express.json())
app.use(cors())

//Routes

// app.use('/api/employees',require('./routes/employee.routes'))            //sirve para crear rutas a los diferentes modelos
app.use('/api/bp_medicamentos', require('./routes/bp_medicamento.routes.js'))
app.use('/api/bg_medicamentos',require('./routes/bg_medicamento.routes'))
app.use('/', require('./routes/usuario.routes.js'))
app.use('/', require('./routes/rol.routes.js'))
app.use('/', require('./routes/receta.routes.js'))
app.use('/', require('./routes/paciente.routes.js'))
app.use('/', require('./routes/indicacion.routes.js'))
app.use('/', require('./routes/bp_operacion.routes.js'))
app.use('/', require('./routes/bg_operacion.routes.js'))
app.use('/', require('./routes/autorizacion.routes'))
app.use('/api/medicamentos', require('./routes/medicamento.routes.js'))


//Starting the server
app.listen(app.get('port'), () => {
    try {
        console.log(`Server opened on port ${app.get('port')}`);
    } catch (error) {
        logger.error(`: en el Server : ${error}`);    
    }
})