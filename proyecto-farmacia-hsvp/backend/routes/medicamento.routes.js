const { Router } = require('express');
const { medicamentoCtrl } = require('../controllers/medicamento.controller');

const router = Router();

router.post('/', medicamentoCtrl.createMedicamentos);

router.get('/', medicamentoCtrl.getMedicamentos);

router.get('/:id', medicamentoCtrl.getMedicamento);

router.put('/:id', medicamentoCtrl.updateMedicamentos);

router.delete('/:id', medicamentoCtrl.deleteMedicamentos);

module.exports = router;

