const express = require('express');
const router = express.Router();

const pacienteController = require('../controllers/paciente.controller.js');

router.get('/paciente/', pacienteController.getPacientes);
router.post('/paciente/', pacienteController.createPaciente);
router.get('/paciente/:id', pacienteController.getPaciente);
router.put('/paciente/:id', pacienteController.editPaciente)
router.delete('/paciente/:id', pacienteController.eliminarPaciente);

module.exports = router