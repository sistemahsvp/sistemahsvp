const express = require('express');
const router = express.Router();

const usuarioController = require('../controllers/usuario.controller.js');

router.get('/usuario/', usuarioController.getUsuarios);
router.post('/usuario/', usuarioController.crearUsuario);
router.get('/usuario/:id', usuarioController.getUsuario);
router.get('/usuario/:id/usuario/:clave', usuarioController.getUsuarioLogin);
router.put('/usuario/:id', usuarioController.actualizarUsuario)
router.delete('/usuario/:id', usuarioController.eliminarUsuario);

module.exports = router