const express = require('express');
const router = express.Router();

const indicacionController = require('../controllers/indicacion.controller.js');

router.get('/indicacion/', indicacionController.getIndicaciones);
router.post('/indicacion/', indicacionController.createIndicacion);
router.get('/indicacion/:id', indicacionController.getIndicacion);
router.put('/indicacion/:id', indicacionController.editIndicacion)
router.delete('/indicacion/:id', indicacionController.eliminarIndicacion);

module.exports = router