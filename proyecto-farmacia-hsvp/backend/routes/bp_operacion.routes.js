const express = require('express');
const router = express.Router();

const bpOperacionController = require('../controllers/bp_operacion.controller.js');

router.get('/bpOperacion/', bpOperacionController.getOperaciones);
router.post('/bpOperacion/', bpOperacionController.createOperacion);
router.get('/bpOperacion/:id', bpOperacionController.getOperacion);
router.put('/bpOperacion/:id', bpOperacionController.editOperacion)
router.delete('/bpOperacion/:id', bpOperacionController.eliminarOperacion);

module.exports = router