const express = require('express');
const router = express.Router();

const bgOperacionController = require('../controllers/bg_operacion.controller.js');

router.get('/bgOperacion/', bgOperacionController.getOperaciones);
router.post('/bgOperacion/', bgOperacionController.createOperacion);
router.get('/bgOperacion/:id', bgOperacionController.getOperacion);
router.put('/bgOperacion/:id', bgOperacionController.editOperacion)
router.delete('/bgOperacion/:id', bgOperacionController.eliminarOperacion);

module.exports = router