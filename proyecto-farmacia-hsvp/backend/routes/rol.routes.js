const express = require('express');
const router = express.Router();

const rolController = require('../controllers/rol.controller.js');

router.get('/rol/', rolController.getRoles);
router.post('/rol/', rolController.crearRol);
router.get('/rol/:id', rolController.getRol);
router.put('/rol/:id', rolController.actualizarRol)
router.delete('/rol/:id', rolController.eliminarRol);

module.exports = router