const express = require('express');
const router = express.Router();

const recetaController = require('../controllers/receta.controller.js');

router.get('/receta/', recetaController.getRecetas);
router.get('/receta/paciente/:identificacion', recetaController.getRecetasPorPaciente)
router.get('/receta/reintegrosPorMes', recetaController.getReintegrosPorMes)
router.get('/receta/recetasCedulaMedicamento/:identificacion/receta/recetasCedulaMedicamento/:cod_medicamento',recetaController.getRecetasPorCedulaMedicamento)
router.post('/receta/', recetaController.createReceta);
router.get('/receta/operacion/mensaje', recetaController.getRecetasMsj);
router.get('/receta/:id', recetaController.getReceta);
router.put('/receta/:id', recetaController.editReceta)
router.delete('/receta/:id', recetaController.eliminarReceta);

//traer receta con msj


module.exports = router