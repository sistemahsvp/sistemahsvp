const { Router } = require('express')
//const res = require('express/lib/response')
const router = Router()

const bpMedicamentosCtrl = require('../controllers/bp_medicamento.controller.js')


//CRUD
router.get('/', bpMedicamentosCtrl.getMedicamentos);

router.put('/:id', bpMedicamentosCtrl.editMedicamento);

router.get('/:id', bpMedicamentosCtrl.getMedicamento);


router.post('/', bpMedicamentosCtrl.createMedicamento);


module.exports = router