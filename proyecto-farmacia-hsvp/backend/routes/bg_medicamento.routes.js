const{ Router } = require('express')
const router = Router()

const bgController = require('../controllers/bg_medicamento.controller.js')

router.get('/',bgController.getbg_medicamentos)
router.get('/consultas/:number',bgController.getLastNMedicamentos)
router.post('/', bgController.createbg_medicamento)
router.get('/:id',bgController.getbg_medicamento)

router.post('/', bgController.createMedicamento);

router.put('/:id', bgController.updatebg_medicamento)

module.exports = router 