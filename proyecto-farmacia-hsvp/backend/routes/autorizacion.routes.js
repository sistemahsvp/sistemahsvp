const express = require('express');
const router = express.Router();

const autorizacionController = require('../controllers/autorizacion.controller.js');

router.get('/autorizacion/', autorizacionController.getAutorizaciones);
router.post('/autorizacion/', autorizacionController.createAutorizacion);
router.get('/autorizacion/:id', autorizacionController.getAutorizacion);
router.put('/autorizacion/:id', autorizacionController.editAutorizacion)
router.delete('/autorizacion/:id', autorizacionController.eliminarAutorizacion);
router.get('/autorizacion/operacion/claveInicio/:clave_inicial', autorizacionController.getAutorizacionClaveInicio);
router.put('/autorizacion/claveInicio/:clave_inicial', autorizacionController.actualizarPorClaveInicial);

router.get('/autorizacion/:cedula/autorizacion/:codMed', autorizacionController.buscarAutorizacionCedMed);

module.exports = router