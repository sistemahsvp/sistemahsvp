const {createLogger, format, transports} = require('winston');


const fecha = new Date();
result = fecha.toLocaleTimeString();
const dia = fecha.getDate();
const mes = fecha.getMonth() + 1;
const year = fecha.getFullYear();
const fechaActual = `${year}-${mes}-${dia}`;

module.exports = createLogger({
    format: format.combine(
        format.simple(),
        format.printf(info => `[Fecha:${fechaActual} Hora:${result}] ${info.level} ${info.message}`)
    ),
    transports: [
        new transports.File({
            maxsize: 5120000,
            maxFiles: 5,
            filename: `${__dirname}/../logs/log-api-log`
        }),
        new transports.Console({
            level: 'debug',
        })
    ]
})