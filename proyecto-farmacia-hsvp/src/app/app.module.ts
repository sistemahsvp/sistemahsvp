import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LoginComponent } from './components/login/login.component';
import { AutorizacionesComponent } from './components/autorizaciones/autorizaciones.component';
import { AdministradorComponent } from './components/administrador/administrador.component';
import { BodegaPasoComponent } from './components/bodega-paso/bodega-paso.component';
import { BodegaGeneralComponent } from './components/bodega-general/bodega-general.component';
import { TecnicosComponent } from './components/tecnicos/tecnicos.component';
import { NavBarComponent } from './components/nav-bar/nav-bar.component';
import { AdministradorModule } from './components/administrador/administrador.module';
import { LoginModule } from './components/login/login.module';
import { AutorizacionesModule } from './components/autorizaciones/autorizaciones.module';
import { BodegaGeneralModule } from './components/bodega-general/bodega-general.module';
import { BodegaPasoModule } from './components/bodega-paso/bodega-paso.module';
import { TecnicosModule } from './components/tecnicos/tecnicos.module';
import { NavBarModule } from './components/nav-bar/nav-bar.module';
import { AdministradorRoutingModule } from './components/administrador/administrador-routing.module';
import { BodegaPasoRoutingModule } from './components/bodega-paso/bodega-paso-routing.module';
import { BodegaGeneralRoutingModule } from './components/bodega-general/bodega-general-routing.module';
import { TecnicosRoutingModule } from './components/tecnicos/tecnicos-routing.module';
import { HttpClientModule } from '@angular/common/http';




@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    AutorizacionesComponent,
    AdministradorComponent,
    BodegaPasoComponent,
    BodegaGeneralComponent,
    TecnicosComponent,
    NavBarComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    AdministradorModule,
    LoginModule,
    AutorizacionesModule,
    BodegaGeneralModule,
    BodegaPasoModule,
    TecnicosModule,
    NavBarModule,
    AdministradorRoutingModule,
    BodegaPasoRoutingModule,
    BodegaGeneralRoutingModule,
    TecnicosRoutingModule,
    HttpClientModule
  ],
  providers: [{ provide: LocationStrategy, useClass: HashLocationStrategy }],
  bootstrap: [AppComponent]
})
export class AppModule { }
