import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { RecetaService } from 'src/app/services/receta.service';
import { Receta } from 'src/app/models/receta';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { BpMedicamentoService } from 'src/app/services/bp-medicamento.service';
import { bpMedicamentoModel } from 'src/app/models/bpMedicamento.model';

@Component({
  selector: 'app-reintegrar',
  templateUrl: '/reintegrar.component.html',
  styleUrls: ['/reintegrar.component.scss']
})
export class ReintegrarComponent implements OnInit {

  _id: string;
  medicamento: String;
  cantidad: Number;
  btn: 'aceptar';
  mostrarLista = false;
  mensaje = '';
  mostrarMensaje = false;
  estado: String;
  recetaModelo: Receta;
  medicamentoBP: bpMedicamentoModel;

  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  constructor(public dialogRef: MatDialogRef<ReintegrarComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public recetaService: RecetaService,
    public bodegaPasoService: BpMedicamentoService,
    public snackBar: MatSnackBar) { }

  ngOnInit(): void {
  }

  consultarCodigoReceta(form: any) {

    this.recetaService.getReceta(form.value._id).subscribe(
      (res) => {

        if (res != undefined || res != null) {
          if (res._id == form.value._id) {
            this.medicamento = res.nom_medicamento;
            this.cantidad = res.cantidad;
            this.estado = res.estado;
            this.mostrarLista = true;
            this.mostrarMensaje = false;

            this.snackBarExito('Consulta de receta exitosa');
          }
        } else if (form.value._id != undefined) {
          if(this._id.length == 7){
            this.mensaje = `Receta con el código: ${form.value._id} no encontrada.`
            this.mostrarMensaje = true;
            this.mostrarLista = false;
          }else{
            this.mostrarMensaje = false;
            this.mostrarLista = false;
          }
        }
      },
      err => {
        console.log(err);
      })
  }

  async verificarExistenciaReintegro() {

    this.recetaService.getReceta(this._id).subscribe(
      (res) => {
        if (res.estado != 'Reintegrada') {
          this.construirReintegro(res);
          this.reintegrarReceta();
        } else {
          this.snackBarError('Receta ya se encuentra reintegrada');
        }
      }
    )
  }

  async reintegrarReceta() {

    this.recetaService.actualizarReceta(this.recetaModelo, this._id).subscribe(
      res => {
        this.actualizarCantidadBP(this.recetaModelo.cod_medicamento as string, this.recetaModelo.cantidad as number)
      },
      err => {
        console.log(err);
      });
    this.mostrarLista = false;
    this._id = '';
  }

  async actualizarCantidadBP(codigo: string, cantidadReintegro: number) {

    this.bodegaPasoService.getMedicametoBp(codigo).subscribe(
      (res) => {
        if (res != undefined || res != null) {
          let total = res.cantidad + cantidadReintegro;
          total = Math.round((total + Number.EPSILON) * 100) / 100;

          this.medicamentoBP = {
            _id: res._id,
            nom_medicamento: res.nom_medicamento,
            cantidad: total
          };

          this.bodegaPasoService.editMedicamento(this.medicamentoBP, codigo).subscribe(
            res => {
              this.snackBarExito('Reintegro exitoso');
            },
            err => {
              console.log(err);
            });
        }
      },
      err => {
        console.log(err);
      });
  }

  async construirReintegro(receta: Receta) {

    this.recetaModelo = {
      _id: this._id,
      cedula: receta.cedula,
      nombre: receta.nombre,
      cod_medicamento: receta.cod_medicamento,
      nom_medicamento: receta.nom_medicamento,
      cod_indicacion: receta.cod_indicacion,
      nom_indicacion: receta.nom_indicacion,
      estado: 'Reintegrada',
      cantidad: receta.cantidad,
      usuario: sessionStorage.getItem('usuario') as String,
      fecha_entrada: receta.fecha_entrada,
      fecha_retiro: receta.fecha_retiro,
      fecha_reintegro: new Date(),
      responsable: receta.responsable,
      mensaje: receta.mensaje
    }
  }

  snackBarExito(mensaje: string) {
    this.snackBar.open(mensaje, '', {
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
      duration: 3000,
      panelClass: 'exito'
    });
  }

  snackBarError(mensaje: string) {
    this.snackBar.open(mensaje, '', {
      horizontalPosition: this.horizontalPosition,
      verticalPosition: this.verticalPosition,
      duration: 3000,
      panelClass: 'advertencia'
    });
  }

  onNoClick() {
    this.dialogRef.close();
  }

}
