import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ReintegrarComponent } from './reintegrar.component';

describe('ReintegrarComponent', () => {
  let component: ReintegrarComponent;
  let fixture: ComponentFixture<ReintegrarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ReintegrarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ReintegrarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
