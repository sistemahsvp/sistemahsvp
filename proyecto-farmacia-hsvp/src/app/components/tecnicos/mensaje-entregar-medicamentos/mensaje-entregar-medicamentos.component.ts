import { Component, OnInit, Inject } from '@angular/core';
import { Receta } from 'src/app/models/receta';
import { RecetaService } from 'src/app/services/receta.service';
import { Autorizacion } from 'src/app/models/autorizacion';
import { AutorizacionService } from 'src/app/services/autorizacion.service';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

@Component({
  selector: 'app-mensaje-entregar-medicamentos',
  templateUrl: '/mensaje-entregar-medicamentos.component.html',
  styleUrls: ['/mensaje-entregar-medicamentos.component.scss']
})
export class MensajeEntregarMedicamentosComponent implements OnInit {

  btn = 'aceptar';

  recetaModelo: Receta;
  recetasModel: Receta[];

  horizontal: MatSnackBarHorizontalPosition = 'end';
  vertical: MatSnackBarVerticalPosition = 'top';

  //Atributos de Receta
  _id: string; //numero de receta
  cedula: Number;
  nombre: String;
  cod_medicamento: String;
  nom_medicamento: String;
  cod_indicacion: Number;
  nom_indicacion: String;
  estado: String;
  cantidad: Number;
  usuario: String;
  fecha_entrada: Date;
  fecha_retiro: Date;
  fecha_reintegro: Date;
  responsable: String;
  mensaje: String;


  //Receta
  eventReceta: String;

  // Validaciones para activar formularios
  validarPaciente = false;



  //Autorizaciones
  //Valores de una Autorización
  cedulaAut: Number;
  nombreAut: String;
  cod_medicamentoAut: String;
  nom_medicamentoAut: String;
  dosis: String;
  diagnostico: String;
  clave_inicial: String;
  clave_continuacion: String;
  medico: String;
  especialidad: String;
  estadoAut: String;
  cantidad_retiros: Number;

  //Modelo
  autorizacionModelo: Autorizacion;
  autorizacionModel: Autorizacion[];
  _idAutorizacion: Number




  constructor(public dialogRef: MatDialogRef<MensajeEntregarMedicamentosComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public recetaService: RecetaService,
    public autorizacionService: AutorizacionService,
    public snackbar: MatSnackBar) {
    this.recetasModel = [];
    this.autorizacionModel = [];
  }

  ngOnInit(): void {

  }

  //Cerrar el mensaje
  onNoClick(): void {
    this.dialogRef.close();
  }


  /*Metodo para que busque por numero de receta si ya se retiro o no  y se traiga los datos:
  la cedula, nombre del paciente, codigo del medicamento, nombre del medicamento y la cantidad */
  buscarNumReceta(form: any) {

    this.recetaService.getReceta(form.value._id).subscribe(
      (res) => {
        if (res != undefined || res != null) {


          if (res.estado != 'Retirada') {

            this.snackbar.open('Receta se encuentra pendiente a retirar', '', {
              horizontalPosition: this.horizontal,
              verticalPosition: this.vertical,
              duration: 3000,
              panelClass: 'Exito'
            });

            this.cedula = res.cedula;
            this.nombre = res.nombre;
            this.cod_medicamento = res.cod_medicamento;
            this.nom_medicamento = res.nom_medicamento;
            this.cantidad = res.cantidad;

          } else {
            this.snackbar.open('Receta ya se retiro', '', {
              horizontalPosition: this.horizontal,
              verticalPosition: this.vertical,
              duration: 3000,
              panelClass: 'Error'
            });
            this.onNoClick();
          }
        } else {
          this.snackbar.open('Receta No Existe', '', {
            horizontalPosition: this.horizontal,
            verticalPosition: this.vertical,
            duration: 3000,
            panelClass: 'Advertencia'
          });
        }
      },
      err => {
        console.log(err);
      })
  }

  /*Metodo que guarda la Receta */
  construirRetiro(receta: Receta) {

    this.recetaModelo = {
      _id: this._id,
      cedula: receta.cedula,
      nombre: receta.nombre,
      cod_medicamento: receta.cod_medicamento,
      nom_medicamento: receta.nom_medicamento,
      cod_indicacion: receta.cod_indicacion,
      nom_indicacion: receta.nom_indicacion,
      estado: 'Retirada',
      cantidad: receta.cantidad,
      usuario: sessionStorage.getItem('usuario') as String,
      fecha_entrada: receta.fecha_entrada,
      fecha_retiro: new Date(),
      fecha_reintegro: receta.fecha_reintegro,
      responsable: this.responsable,
      mensaje: receta.mensaje
    }
  }

  //Metodo para realizar la entrega de la Receta
  retirarReceta() {

    this.recetaService.actualizarReceta(this.recetaModelo, this._id).subscribe(
      (res) => {
        if (res != undefined || res != null) {

          this.snackbar.open('Receta Entregada con exito', '', {
            horizontalPosition: this.horizontal,
            verticalPosition: this.vertical,
            duration: 3000,
            panelClass: 'Exito'
          });

        } else {
          this.snackbar.open('Error al Entregar Receta', '', {
            horizontalPosition: this.horizontal,
            verticalPosition: this.vertical,
            duration: 3000,
            panelClass: 'Error'
          });
        }
      },
      err => {
        console.log(err);
      });
  }


  guardarRetiro() {
    this.recetaService.getReceta(this._id).subscribe(
      (res) => {
        if (res.estado != 'Retirada') {
          this.construirRetiro(res);
          this.retirarReceta();
          this.restarRetiro();

        } else {
          this.snackbar.open('Receta ya se retiro', '', {
            horizontalPosition: this.horizontal,
            verticalPosition: this.vertical,
            duration: 3000,
            panelClass: 'Advertencia'
          });
        }


      }
    )
  }




  restarRetiro() {

    this.autorizacionService.getAutorizacionCedMed(this.cedula, this.cod_medicamento).subscribe(
      (res) => {
        if (res.length != 0) {
          let autoriza = res.pop();

          if (autoriza?.estado == 'Autorizado') {

            let nuevaCantidad = autoriza?.cantidad_retiros as number - 1
            autoriza.cantidad_retiros = nuevaCantidad

            if (nuevaCantidad == 0) {
              autoriza.estado = 'Finalizado'

            }

            this.autorizacionService.actualizarAutorizacionPorClaveInicial(autoriza, autoriza.clave_inicial).subscribe(

            )

          }
        }
      }
    )
  }



}
