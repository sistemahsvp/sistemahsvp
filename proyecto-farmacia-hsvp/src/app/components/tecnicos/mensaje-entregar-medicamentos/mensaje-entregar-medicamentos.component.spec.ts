import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MensajeEntregarMedicamentosComponent } from './mensaje-entregar-medicamentos.component';

describe('MensajeEntregarMedicamentosComponent', () => {
  let component: MensajeEntregarMedicamentosComponent;
  let fixture: ComponentFixture<MensajeEntregarMedicamentosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MensajeEntregarMedicamentosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MensajeEntregarMedicamentosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
