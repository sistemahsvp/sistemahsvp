import { Component, OnInit, Inject, Input } from '@angular/core';

import { Receta } from 'src/app/models/receta';
import { RecetaService } from 'src/app/services/receta.service';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
//Paciente
import { Paciente } from '../../../models/paciente';
import { PacienteService } from 'src/app/services/paciente.service';
//Medicamentos BP
import { bpMedicamentoModel } from 'src/app/models/bpMedicamento.model';
import { BpMedicamentoService } from 'src/app/services/bp-medicamento.service';
//Indicacion
import { Indicacion } from '../../../models/indicacion';
import { IndicacionService } from '../../../services/indicacion.service';
import { _isNumberValue } from '@angular/cdk/coercion';


@Component({
  selector: 'app-mensaje-registrar',
  templateUrl: '/mensaje-registrar.component.html',
  styleUrls: ['/mensaje-registrar.component.scss']
})
export class MensajeRegistrarComponent implements OnInit {

  //Mostrar
  mostrarForm1 = false
  mostrarMensaje = false;

  btnRegi = 'aceptar';
  nombreCompleto: string;
  recetaModelo: Receta;

  // Validaciones para activar formularios
  validarPaciente = false;
  validarMedicamento = false;
  validarIndicacion = false;

  validarReceta = false;

  //Atributos de Receta
  _id: string; //numero de receta
  cedula: number; //_id - Paciente
  nombre: String; //nombre del paciente
  cod_medicamento: string; //_id codigo del medicamento(Bodega de Paso)
  nom_medicamento: String; //nombre del medicaneto(Bodega de Paso)
  cod_indicacion: Number; //_id codigo de la indicacion(Indicacion)
  nom_indicacion: String; //nombre del pacient(Indicacion)
  estado: String;
  cantidad: Number;
  usuario: String;
  fecha_entrada: Date;
  fecha_retiro: Date;
  fecha_reintegro: Date;
  responsable: String;
  mensaje = "";

  //Receta
  eventReceta: String;

  //Atributos de Paciente
  apellido1: String;
  apellido2: String;

  //Modelos
  recetasModel: Receta[];
  pacienteModel: Paciente[];
  medicamentoBpModel: bpMedicamentoModel[];
  indicacionModel: Indicacion[];

  horizontal: MatSnackBarHorizontalPosition = 'end';
  vertical: MatSnackBarVerticalPosition = 'top';

  constructor(
    public dialogRef: MatDialogRef<MensajeRegistrarComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public recetaService: RecetaService,
    public pacienteService: PacienteService,
    public medicamentoBpService: BpMedicamentoService,
    public indicacionService: IndicacionService,
    public snackbar: MatSnackBar,
    public dialog: MatDialog
  ) {
    this.mensaje = data.mensaje;
    this.recetasModel = [];
    this.pacienteModel = [];
    this.medicamentoBpModel = [];
    this.indicacionModel = [];

  }

  ngOnInit(): void {
  }

  //Cerrar el mensaje
  onNoClick(): void {
    this.dialogRef.close();
  }

  //Metodo para validar Receta existente
  buscarReceta(eventReceta: any) {
    this._id = eventReceta;
    this.recetaService.getReceta(this._id).subscribe(
      (res) => {
        if (res != undefined || res != null) {
          this.snackbar.open('Número de Receta ya existe', '', {
            horizontalPosition: this.horizontal,
            verticalPosition: this.vertical,
            duration: 3000,
            panelClass: 'Advertencia'
          });
          this.onNoClick()
        } else {
          this.snackbar.open('Número de Receta Válido', '', {
            horizontalPosition: this.horizontal,
            verticalPosition: this.vertical,
            duration: 4000,
            panelClass: 'Exito'
          });
        }
      },
      err => {
        console.log(err);
      }
    )
    return false
  }



  //Método que me trae el Nombre del Paciente segun la cedula
  buscarPaciente() {

    if (isNaN(this.cedula)) {

      this.nombre = 'No encontrado';

    } else {

      //Servicio
      if (this.cedula.toString() != '') {
        if (this.cedula != null) {
          this.pacienteService.getPaciente(this.cedula).subscribe(
            (res) => {
              if (res != undefined) {
                //Atributos de Paciente
                this.nombre = res.nombre
                this.apellido1 = res.apellido1
                this.apellido2 = res.apellido2
                //Concatenacion de Nombre
                this.nombre = `${this.nombre} ${this.apellido1} ${this.apellido2}`;
                this.validarPaciente = true;

              } else {
                this.nombre = 'No encontrado';
              }
            }
          )
        }
      } else { }

    }
  }



  //Método que me trae el Nombre(Medicamento) del Paciente segun el Codigo
  buscarMedicamento() {
    this.reconocerReceta()
    if(this.cod_medicamento != undefined){
    this.medicamentoBpService.getMedicametoBp(this.cod_medicamento).subscribe(
      (res) => {
        if (res != undefined) {
          this.nom_medicamento = res.nom_medicamento

          this.validarMedicamento = true;
        } else {
          this.nom_medicamento = 'No encontrado';
        }
      }
    )}
  }



  //Método que me trae el Nombre de la Indicacion segun el Codigo
  buscarIndicacion() {
    if (isNaN(this.cod_indicacion as number) || this.cod_indicacion == undefined) {

      this.nom_indicacion = 'No encontrado';


    } else {

      //servicio
      this.indicacionService.getIndicacion(this.cod_indicacion).subscribe(     
        (res) => {          
          if (res != undefined) {
            this.nom_indicacion = res.nom_indicacion

            this.validarIndicacion = true;
          } else {
            this.nom_indicacion = 'No encontrado';
          }
        }
      )
    }
  }


  //Metodo para guardar la Receta
  construirRegistrar() {

    this.recetaModelo = {
      _id: this._id,
      cedula: this.cedula,
      nombre: this.nombre,
      cod_medicamento: this.cod_medicamento,
      nom_medicamento: this.nom_medicamento,
      cod_indicacion: this.cod_indicacion,
      nom_indicacion: this.nom_indicacion,
      estado: 'Pendiente',
      cantidad: this.cantidad,
      usuario: sessionStorage.getItem('usuario') as String,
      fecha_entrada: new Date(),
      fecha_retiro: new Date(0),
      fecha_reintegro: new Date(0),
      responsable: '-',
      mensaje: this.mensaje
    }
  }


  //Método para registrar la receta
  registrarReceta(form: any) {
    if (this.nom_medicamento != undefined) {
      this.construirRegistrar();
      this.recetaService.getReceta(this._id).subscribe(
        res => {
          if(res == null || res == undefined){
            this.recetaService.crearReceta(this.recetaModelo).subscribe(
              res => {
                this.snackbar.open('Receta registrada correctamente', '', {
                  horizontalPosition: this.horizontal,
                  verticalPosition: this.vertical,
                  duration: 3000,
                  panelClass: 'Exito'
                })
              },
              err => {
                this.snackbar.open('Error al registrar Receta', '', {
                  horizontalPosition: this.horizontal,
                  verticalPosition: this.vertical,
                  duration: 3000,
                  panelClass: 'Error'
                })
              }
            )
          }else{
            this.snackbar.open('Número de Receta ya existe', '', {
              horizontalPosition: this.horizontal,
              verticalPosition: this.vertical,
              duration: 3000,
              panelClass: 'Advertencia'
            });
          }
        },
        err => {
          console.log(err);
        });
    }
  }


  //Metodo para Reconocer Receta segun Cedula, Medicamento 
  reconocerReceta() {
    const fechaValida = (new Date().getUTCMonth()) - 1
    let mensajeReceta: Boolean;
    if(this.cedula != undefined && this.cod_medicamento !=undefined){
    this.recetaService.getRecetasPorCedulaMedicamento(this.cedula, this.cod_medicamento).subscribe(
      (res) => {
        if (res.length != 0) {
          this.mostrarMensaje = true;
        } else {
          this.mostrarMensaje = false;
        }
        res.forEach(receta => {
          const fechaEntrada = new Date(receta.fecha_entrada)
          //console.log(`fechaEntrada ${fechaEntrada}`);

          if (fechaEntrada.getMonth() == fechaValida && receta.cod_medicamento == this.cod_medicamento) {
            this.mostrarMensaje = true;
          } else {
            mensajeReceta = false

          }
        })
      }
    )}
  }



}
