import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MensajeRegistrarComponent } from './mensaje-registrar.component';

describe('MensajeRegistrarComponent', () => {
  let component: MensajeRegistrarComponent;
  let fixture: ComponentFixture<MensajeRegistrarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MensajeRegistrarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MensajeRegistrarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
