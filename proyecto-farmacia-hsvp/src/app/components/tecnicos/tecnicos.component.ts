import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { ReintegrarComponent } from './reintegrar/reintegrar.component';
import { MatTableDataSource } from '@angular/material/table';
import { MensajeRegistrarComponent } from './mensaje-registrar/mensaje-registrar.component';
import { MensajeEntregarMedicamentosComponent } from './mensaje-entregar-medicamentos/mensaje-entregar-medicamentos.component';
import { RecetaService } from '../../services/receta.service';
import { ConsultaAutComponent } from './consulta-aut/consulta-aut.component';

@Component({
  selector: 'app-tecnicos',
  templateUrl: '/tecnicos.component.html',
  styleUrls: ['/tecnicos.component.scss']
})
export class TecnicosComponent implements OnInit {

  dataSource = new MatTableDataSource<any>([]);

  //lo utilizo para enviar el id y mostrarlo en el mensaje
  _id: string;

  constructor(public RecetaService: RecetaService,
    public dialog: MatDialog) {
    this.getRecetas();
  }

  ngOnInit(): void {
    this.getRecetas();

  }

  getRecetas() {
    /*this.RecetaService.getRecetas.subscribe(
      res => {
        this.dataSource.data = res
      },
      err => console.error(err)
      )*/
  }

  abrirMensajeRegistrar(): void {
    const dialogRef = this.dialog.open(MensajeRegistrarComponent, {
      panelClass: 'custom-dialog-container',
      maxHeight: window.innerHeight + 'px',
      width: '650px',
      data: { _id: this._id },
    })
  }

  abrirMensajeEntregarMedicamentos(): void {
    const dialogRef = this.dialog.open(MensajeEntregarMedicamentosComponent, {
      panelClass: 'custom-dialog-container',
      maxHeight: window.innerHeight + 'px',
      width: '560px',
      data: {},
    })
  }

  mensajeReintegro() {
    const dialogRef = this.dialog.open(ReintegrarComponent, {
      width: '350px',
      data: {},
    });
  }

  mensajeConsultaAutorizacion(){
    const dialogRef = this.dialog.open(ConsultaAutComponent, {
      width: '900px',
      data: {}
    })
  }
}
