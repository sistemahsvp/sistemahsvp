import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConsultaAutComponent } from './consulta-aut.component';

describe('ConsultaAutComponent', () => {
  let component: ConsultaAutComponent;
  let fixture: ComponentFixture<ConsultaAutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConsultaAutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ConsultaAutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
