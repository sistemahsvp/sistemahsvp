import { Component, OnInit, Inject, ViewChild} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AutorizacionService } from 'src/app/services/autorizacion.service';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator} from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

@Component({
  selector: 'app-consulta-aut',
  templateUrl: '/consulta-aut.component.html',
  styleUrls: ['/consulta-aut.component.scss']
})
export class ConsultaAutComponent implements OnInit {

  displayedColumns: string[] = ['cedula', 'nombre','cod_medicamento', 'nom_medicamento',
   'estado', 'cantidad'];
  dataSource = new MatTableDataSource<any>([]);

  cedula: Number;
  cod_medicamento: String;
  mostrarLista = false;
  mostrarMensaje = false;
  nombre: String;
  nom_medicamento: String;
  dosis: String;
  estado: String;
  mensaje: String;

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(public dialogRef: MatDialogRef<ConsultaAutComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public autorizacionService: AutorizacionService) {
  }

  ngOnInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  consultarAut(){
    this.autorizacionService.getAutorizacionCedMed(this.cedula, this.cod_medicamento).subscribe(
      res => {
        if(res.length != 0){
          this.dataSource.data = res;
          this.mostrarLista = true;
          this.mostrarMensaje = false;
        }else {
          this.mensaje = `La autorización con la cédula ${this.cedula} 
          y con el código de medicamento ${this.cod_medicamento} no fue encontrada`
          this.mostrarMensaje = true;
          this.mostrarLista = false;
        }  
      },
      err => {
        console.log(err);
        this.mostrarLista = false;
      });
  }

  onNoClick() {
    this.dialogRef.close();
  }
}
