import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConsultarComponent } from './consultar/consultar.component';
import { ConsultaAutComponent } from './consulta-aut/consulta-aut.component';
import { ReintegrarComponent } from './reintegrar/reintegrar.component';

const routes: Routes = [
  { path: 'consultarPerfil', component: ConsultarComponent },
  { path: 'consultaAut', component: ConsultaAutComponent },
  { path: 'reintegrar', component: ReintegrarComponent },
]
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class TecnicosRoutingModule { }





