import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { Paciente } from 'src/app/models/paciente';
import { PacienteService } from 'src/app/services/paciente.service';
import { RecetaService } from 'src/app/services/receta.service';
import { Receta } from 'src/app/models/receta';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';


@Component({
  selector: 'app-consultar',
  templateUrl: '/consultar.component.html',
  styleUrls: ['/consultar.component.scss']
})
export class ConsultarComponent implements OnInit {
  constructor(private pacienteService: PacienteService, private recetaService: RecetaService, public snackbar: MatSnackBar) {
    this.nombre = '';
  }
  displayedColumns: string[] = ['_id', 'nom_medicamento', 'cantidad', 'estado', 'tecnico', 'responsable'];
  dataSource = new MatTableDataSource<any>([]);

  @ViewChild(MatPaginator, { static: true })
  paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true })
  sort!: MatSort;

  identificacion: number;
  nombre: string;
  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  ngOnInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  buscarPaciente() {
    if(this.identificacion != undefined || this.identificacion != null){
      this.pacienteService.getPaciente(this.identificacion).subscribe(res => {
        if (res != undefined) {
          this.nombre = `${res.nombre} ${res.apellido1} ${res.apellido2}`
          this.recetaService.getRecetasPorPaciente(this.identificacion).subscribe(res => {
  
            this.dataSource.data = res;
            this.snackbar.open(`Resultados cargados`, '', {
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
              duration: 2000,
              panelClass: ['exito']
            });
          })
        } else {
          this.nombre = ''
          this.dataSource.data = []
          this.recetaService.getRecetasPorPaciente(this.identificacion).subscribe(res => {
  
            this.dataSource.data = res;
            this.snackbar.open(`Resultados no encontrados`, '', {
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
              duration: 2000,
              panelClass: ['msg-error']
            });
          })
        }
      });
    }   
  }


}
