import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TecnicosComponent } from '../tecnicos.component';


@NgModule({
  declarations: [
    TecnicosComponent
  ],
  imports: [
    CommonModule,
  ],
  exports: [
  ]
})
export class ConsultarModule { }
