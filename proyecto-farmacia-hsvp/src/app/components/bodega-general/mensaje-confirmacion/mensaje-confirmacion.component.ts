import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { BodegaGeneralService } from 'src/app/services/bodega-general.service';
import { BodegaGeneralComponent } from '../bodega-general.component';

@Component({
  selector: 'app-mensaje-confirmacion',
  templateUrl: '/mensaje-confirmacion.component.html',
  styleUrls: ['/mensaje-confirmacion.component.scss']
})
export class MensajeConfirmacionComponent implements OnInit {

  nuevaCantidad: number;
  btn: 'aceptar';
  _id: String;
  cantidadActual: number;
  medicamentosBG: any;
  cantidad: number;
  mensaje: string;
  accion: string;

  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  BGComponent: BodegaGeneralComponent;

  constructor(public dialogRef: MatDialogRef<MensajeConfirmacionComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public bgMedicamentoService: BodegaGeneralService,
    public snackBar: MatSnackBar) {
    this.mensaje = this.data.mensaje;
  }

  ngOnInit(): void {
    if (this.data.accion === 'Agregar') {
      this.accion = 'Agregar';
    } else if (this.data.accion === 'Despachar') {
      this.accion = 'Despachar';
    }
  }

  actualiarCantidad() {

    if (this.data.accion === 'Agregar') {

      this.sumarRestar(this.data.accion);

      this.llenarModelo();

      this.agregarCantidad();

    } else if (this.data.accion === 'Despachar') {

      if (this.nuevaCantidad <= this.data.cantidadActual) {

        this.sumarRestar(this.data.accion);

        this.llenarModelo();

        this.despacharCantidad();

      } else {
        this.snackBar.open('No se puede despachar una cantidad mayor a la existente', '', {
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
          duration: 3000,
          panelClass: 'advertencia'
        });
      }
    }
  }

  sumarRestar(operador: string) {
    if (operador === 'Agregar') {
      this.cantidad = this.data.cantidadActual + this.nuevaCantidad;
      // Number.EPSILON asegura el redondeo exacto del número.
      // Multiplicado por 100 para sacar los 2 dígitos después del punto decimal.
      // Dividido entre 100 para obtener como máximo 2 lugares decimales.
      this.cantidad = Math.round((this.cantidad + Number.EPSILON) * 100) / 100;

    } else {
      this.cantidad = this.data.cantidadActual - this.nuevaCantidad;
      this.cantidad = Math.round((this.cantidad + Number.EPSILON) * 100) / 100;

      if (this.cantidad < 0) {
        this.cantidad = 0;
      }
    }
  }

  llenarModelo() {
    this.medicamentosBG = {
      _id: this.data._id,
      nom_medicamento: this.data.nom,
      cantidad: this.cantidad
    }
  }

  agregarCantidad() {
    if (this.nuevaCantidad > 0) {
      this.bgMedicamentoService.updateCantidadBG(this.medicamentosBG, this.data._id).subscribe(
        res => {
          this.snackBar.open('Cantidad agregada con éxito', '', {
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
            panelClass: 'exito'
          });

          setTimeout(function () {
            window.location.reload();
          }, 1000);
        },
        err => {
          console.log(err);
        }
      )
    } else {
      this.snackBar.open('La cantidad no puede ser menor o igual a 0', '', {
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
        duration: 3000,
        panelClass: 'advertencia'
      });
    }
  }

  despacharCantidad() {
    if (this.nuevaCantidad > 0) {
      this.bgMedicamentoService.updateCantidadBG(this.medicamentosBG, this.data._id).subscribe(
        res => {
          this.snackBar.open('Cantidad despachada con éxito', '', {
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
            panelClass: 'exito'
          });

          setTimeout(function () {
            window.location.reload();
          }, 1000);
        },
        err => {
          console.log(err);
        }
      )
    } else {
      this.snackBar.open('La cantidad a despachar no puede ser menor o igual a 0', '', {
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
        duration: 3000,
        panelClass: 'advertencia'
      });
    }
  }


  onNoClick(): void {
    this.dialogRef.close();
  }

}
