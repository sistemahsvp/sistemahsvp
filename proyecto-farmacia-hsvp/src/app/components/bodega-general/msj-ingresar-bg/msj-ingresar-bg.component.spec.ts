import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MsjIngresarBGComponent } from './msj-ingresar-bg.component';

describe('MsjIngresarBGComponent', () => {
  let component: MsjIngresarBGComponent;
  let fixture: ComponentFixture<MsjIngresarBGComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MsjIngresarBGComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MsjIngresarBGComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
