import { Component, OnInit, Inject } from '@angular/core';

//importar el modelo de BG
import { MedicamentosBG } from 'src/app/models/bodega-general';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MedicamentoService } from 'src/app/services/medicamento.service';
import { BodegaGeneralService } from 'src/app/services/bodega-general.service';


@Component({
  selector: 'app-msj-ingresar-bg',
  templateUrl: '/msj-ingresar-bg.component.html',
  styleUrls: ['/msj-ingresar-bg.component.scss']
})
export class MsjIngresarBGComponent implements OnInit {

  mensaje: string;
  btnBG = 'aceptar';

  _id: string;
  nom_medicamento: string;
  cantidad: number;

  medicamentosBG: MedicamentosBG;

  horizontal: MatSnackBarHorizontalPosition = 'end';
  vertical: MatSnackBarVerticalPosition = 'top';


  constructor(
    public dialogRef: MatDialogRef<MsjIngresarBGComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public medicamentoService: MedicamentoService,
    public bodegaGeneralService: BodegaGeneralService,
    public snackbar: MatSnackBar
  ) {
    this.mensaje = data.mensaje;
  }

  ngOnInit(): void {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

  //método que me trae el nombre del medicamento segun el id
  bgIngresoMedicamento() {
    this.medicamentoService.getMedicamento(this._id).subscribe(
      (res) => {
        if (res != undefined) {
          this.nom_medicamento = res.nom_medicamento
        } else {
          this.snackbar.open('Código de Medicamento No es Válido', '', {
            horizontalPosition: this.horizontal,
            verticalPosition: this.vertical,
            duration: 3000,
            panelClass: 'Advertencia'
          })
        }
      }
    )
    this.bodegaGeneralService.getbg_medicamento(this._id).subscribe(
      (res) => {
        if (res != undefined) {
          this.snackbar.open('Medicamento ya existe en Bodega General', '', {
            horizontalPosition: this.horizontal,
            verticalPosition: this.vertical,
            duration: 3000,
            panelClass: 'Advertencia'
          })
          this.onNoClick()
        }
      }
    )
  }


  //segun el id y nombre de medicamento se agrega la cantidad q el usuario indica en el form
  //se agrega al documento de BG_Medicamentos
  agregarMedicamentoBG(form: any) {
    if (form.value.nom_medicamento != undefined) {
      this.medicamentosBG = {
        _id: this._id,
        nom_medicamento: this.nom_medicamento,
        cantidad: this.cantidad
      }

      this.bodegaGeneralService.crearMedicamentoBG(this.medicamentosBG).subscribe(
        res => {
          this.snackbar.open('Medicamento ingresado correctamente', '', {
            horizontalPosition: this.horizontal,
            verticalPosition: this.vertical,
            panelClass: 'Exito'
          });

          setTimeout(function () {
            window.location.reload();
          }, 1000);
        },
        err => {
          this.snackbar.open('Error al ingresar Medicamento', '', {
            horizontalPosition: this.horizontal,
            verticalPosition: this.vertical,
            duration: 3000,
            panelClass: 'Error'
          })
        }
      )
    }
  }
}
