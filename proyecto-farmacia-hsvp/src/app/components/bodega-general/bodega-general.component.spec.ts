import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BodegaGeneralComponent } from './bodega-general.component';

describe('BodegaGeneralComponent', () => {
  let component: BodegaGeneralComponent;
  let fixture: ComponentFixture<BodegaGeneralComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BodegaGeneralComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BodegaGeneralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
