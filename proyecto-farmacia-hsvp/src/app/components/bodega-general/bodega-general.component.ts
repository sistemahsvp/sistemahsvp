import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { BodegaGeneralService } from '../../services/bodega-general.service';
import { MatDialog } from '@angular/material/dialog';
import { MensajeConfirmacionComponent } from '../bodega-general/mensaje-confirmacion/mensaje-confirmacion.component';


//importa el modelo de medicamento para traer el nombre y cod
import { Medicamento } from 'src/app/models/medicamento';
import { MedicamentoService } from 'src/app/services/medicamento.service';
import { MsjIngresarBGComponent } from './msj-ingresar-bg/msj-ingresar-bg.component';

//Bodega de Paso
import { BpMedicamentoService } from 'src/app/services/bp-medicamento.service';



export interface registroTabla {
  _id: string;
  nom_medicamento: string;
  cantidad: number;
  cantidadBp: number;
}

@Component({
  selector: 'app-bodega-general',
  templateUrl: '/bodega-general.component.html',
  styleUrls: ['/bodega-general.component.scss']
})
export class BodegaGeneralComponent implements OnInit {
  displayedColumns: string[] = ['_id', 'nom_medicamento', 'cantidad', 'cantidadBp', 'agregar', 'despachar'];
  dataSource = new MatTableDataSource<any>([]);

  //lo utilizo para enviar el id y mostrarlo en el mensaje
  _id: string;

  @ViewChild(MatPaginator, { static: true })
  paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true })
  sort!: MatSort;


  bpIngresoMedicamento: Medicamento[];

  constructor(public BodegaGeneralService: BodegaGeneralService,
    public BodegaPasoService: BpMedicamentoService,
    public dialog: MatDialog,
    public medicamentoService: MedicamentoService,) {
    this.getbodega_general();
    this.BodegaPasoService
    this.bpIngresoMedicamento;
  }


  ngOnInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  datosTabla = <any>[];
  getbodega_general() {
    this.BodegaGeneralService.getbodega_general().subscribe(
      (res) => {
        res.forEach(medicamentoBG => {
          this.BodegaPasoService.getMedicametoBp(medicamentoBG._id).subscribe(

            medicamentoBP => {

              if (medicamentoBP != null) {
                this.datosTabla.push({
                  _id: medicamentoBG._id,
                  nom_medicamento: medicamentoBG.nom_medicamento,
                  cantidad: medicamentoBG.cantidad,
                  cantidadBp: medicamentoBP.cantidad,
                })
              } else {
                this.datosTabla.push({
                  _id: medicamentoBG._id,
                  nom_medicamento: medicamentoBG.nom_medicamento,
                  cantidad: medicamentoBG.cantidad,
                  cantidadBp: 0,
                })
              }
              this.dataSource.data = this.datosTabla
            }
          )
        })
      },
      err => console.error(err)
    )
  }


  //traer el nombre de medicamento segun el id q trae
  getMedicamento(_id: string) {
    this.BodegaGeneralService.getMedicamento(_id).subscribe(
      res => {
        this.getbodega_general();
      },
      err => console.error(err)
    )
  }

  abrirMensaje(_id: string, cantidadActual: number, nom: string, accion: string) {
    if (accion === 'Agregar') {

      const dialogRef = this.dialog.open(MensajeConfirmacionComponent, {
        width: '280px',
        data: { _id, cantidadActual, nom, mensaje: `Medicamento: ${nom}`, accion },
      });

     


    } else if (accion === 'Despachar') {

      const dialogRef = this.dialog.open(MensajeConfirmacionComponent, {
        width: '280px',
        data: { _id, cantidadActual, nom, mensaje: `Medicamento: ${nom}`, accion },
      });

     
    }
  }


  openDialogBG(): void {
    const dialogRef = this.dialog.open(MsjIngresarBGComponent, {
      width: '250px',
      data: { _id: this._id },
    })

   
  }
}
