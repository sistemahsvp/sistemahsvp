import { Component, OnInit } from '@angular/core';
import { Usuario } from 'src/app/models/usuario';
import { UsuarioService } from 'src/app/services/usuario.service';
import { Router} from '@angular/router'
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';

@Component({
  selector: 'app-login',
  templateUrl: '/login.component.html',
  styleUrls: ['/login.component.scss']
})
export class LoginComponent implements OnInit{
  usuario: string;
  clave: string;
  usuarioModel: Usuario;
  encontrado:boolean;
  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';  

  constructor(private usuarioService: UsuarioService,
      private router: Router, public snackBar: MatSnackBar
    ) { 
    
  }

  validarCredenciales(usuario: string, clave: string){
    if(usuario === undefined || clave === undefined){
      return false
    }else{
      if(usuario.length < 1 || clave.length < 1){
        return false
      }
      return true
    }
  }

  iniciarSesion(){

    if(this.validarCredenciales(this.usuario, this.clave)){
      this.usuarioService.getUsuarioLogin(this.usuario, this.clave)
        .subscribe((res: Usuario)=>{
          if(res._id != undefined && res.clave != undefined){
          this.validar(res)
          }else{
            this.snackBar.open(`Credenciales incorrectas`, '', {
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
              duration: 3000,
              panelClass: ['error']
            });
          }
        }
        )
    }else{
      this.snackBar.open(`Error al iniciar sesion`, '', {
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
        duration: 3000,
        panelClass: ['error']
      });
    }


  }

  validar(usuarioModel: Usuario) {
    if(usuarioModel._id === this.usuario && usuarioModel.clave === this.clave){
      sessionStorage.setItem('usuario', `${usuarioModel._id}`);
      sessionStorage.setItem('rol', `${usuarioModel.rol}`);
      this.redireccionar(usuarioModel.rol);     
    }else{
      this.snackBar.open(`Contraseña Incorrecta`, '', {
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
        duration: 3000,
        panelClass: ['error']
      });
      
    }
  }

  redireccionar(rol: number){
    if(rol == 0){
      this.router.navigate(['/administrador']);
    }else if (rol == 1){
      this.router.navigate(['/autorizaciones']);
    }else if (rol == 2){
      this.router.navigate(['/tecnicos']);
    }else if (rol == 3){
      this.router.navigate(['/bodegaGeneral']);
    }else if (rol == 4){
      this.router.navigate(['/bodegaPaso']);
    }
  }

  ngOnInit(): void {
    document.body.className = "cover";
  }

  ngOnDestroy(){
    document.body.className="";
  }

}

