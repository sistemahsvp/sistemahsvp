import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MensajeDespacharComponent } from './mensaje-despachar.component';

describe('MensajeDespacharComponent', () => {
  let component: MensajeDespacharComponent;
  let fixture: ComponentFixture<MensajeDespacharComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MensajeDespacharComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MensajeDespacharComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
