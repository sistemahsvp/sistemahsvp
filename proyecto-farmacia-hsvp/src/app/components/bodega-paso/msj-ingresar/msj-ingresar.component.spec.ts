import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MsjIngresarComponent } from './msj-ingresar.component';

describe('MsjIngresarComponent', () => {
  let component: MsjIngresarComponent;
  let fixture: ComponentFixture<MsjIngresarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MsjIngresarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MsjIngresarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
