import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MedicamentoService } from 'src/app/services/medicamento.service';
import { BpMedicamentoService } from 'src/app/services/bp-medicamento.service';
import { bpMedicamentoModel } from 'src/app/models/bpMedicamento.model';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { BodegaGeneralService } from 'src/app/services/bodega-general.service';


@Component({
  selector: 'app-msj-ingresar',
  templateUrl: '/msj-ingresar.component.html',
  styleUrls: ['/msj-ingresar.component.scss']
})


export class MsjIngresarComponent implements OnInit {

  mensaje: string;
  btn = 'aceptar';

  _id: string;
  nom_medicamento: string;
  cantidad: number;

  BpMedicamentoModel: bpMedicamentoModel;

  horizontal: MatSnackBarHorizontalPosition = 'end';
  vertical: MatSnackBarVerticalPosition = 'top';

  constructor(
    public dialogRef: MatDialogRef<MsjIngresarComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public medicamentoService: MedicamentoService,
    public bpMedicamentoService: BpMedicamentoService,
    public bgMedicamentoService: BodegaGeneralService,
    public snackbar: MatSnackBar
  ) {
    this.mensaje = data.mensaje;
  }

  ngOnInit(): void {

  }

  onNoClick(): void {
    this.dialogRef.close();
  }


  bpIngresoMedicamento() {
    this.medicamentoService.getMedicamento(this._id).subscribe(
      (res) => {
        if (res != undefined) {
          this.nom_medicamento = res.nom_medicamento
        } else {
          this.snackbar.open('Código de Medicamento No es Válido', '', {
            horizontalPosition: this.horizontal,
            verticalPosition: this.vertical,
            duration: 3000,
            panelClass: 'Advertencia'
          })
        }
      }
    )
    this.bpMedicamentoService.getMedicametoBp(this._id).subscribe(
      (res) => {
        if (res != undefined) {
          this.snackbar.open('Medicamento ya existe en Bodega de Paso', '', {
            horizontalPosition: this.horizontal,
            verticalPosition: this.vertical,
            duration: 3000,
            panelClass: 'Advertencia'
          })
          this.onNoClick()
        }
      }
    )
  }

  agregarBpMedicamento(form: any) {
    if (form.value.nom_medicamento != undefined) {
      this.BpMedicamentoModel = {
        _id: this._id,
        nom_medicamento: this.nom_medicamento,
        cantidad: this.cantidad
      }

      this.bgMedicamentoService.getbg_medicamento(this._id).subscribe(
        res => {
          if (res != undefined || res != null){
            this.bpMedicamentoService.crearbpMedicamento(this.BpMedicamentoModel).subscribe(
              res => {
                this.snackbar.open('Medicamento ingresado correctamente', '', {
                  horizontalPosition: this.horizontal,
                  verticalPosition: this.vertical,
                  panelClass: 'Exito'
                });
      
                setTimeout(function () {
                  window.location.reload();
                }, 1000);
              },
              err => {
                this.snackbar.open('Error al ingresar Medicamento', '', {
                  horizontalPosition: this.horizontal,
                  verticalPosition: this.vertical,
                  panelClass: 'Error'
                })
              }
            )
          }else {
            this.snackbar.open('Medicamento no Existe en Bodega General', '', {
              horizontalPosition: this.horizontal,
              verticalPosition: this.vertical,
              duration: 3000,
              panelClass: 'Advertencia'
            })
          }
        }
      )    
    }
  }
}
