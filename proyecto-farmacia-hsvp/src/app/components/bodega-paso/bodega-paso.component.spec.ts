import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BodegaPasoComponent } from './bodega-paso.component';

describe('BodegaPasoComponent', () => {
  let component: BodegaPasoComponent;
  let fixture: ComponentFixture<BodegaPasoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BodegaPasoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BodegaPasoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
