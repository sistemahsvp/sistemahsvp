import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MensajeEditarComponent } from './mensaje-editar.component';

describe('MensajeEditarComponent', () => {
  let component: MensajeEditarComponent;
  let fixture: ComponentFixture<MensajeEditarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MensajeEditarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MensajeEditarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
