import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { BpMedicamentoService } from 'src/app/services/bp-medicamento.service';

@Component({
  selector: 'app-mensaje-editar',
  templateUrl: '/mensaje-editar.component.html',
  styleUrls: ['/mensaje-editar.component.scss']
})
export class MensajeEditarComponent implements OnInit {

  nuevaCantidad: number;
  btn: 'aceptar';
  _id: String;
  cantidadActual: number;
  bpMedicamento: any;
  cantidad: number;
  mensaje: string;

  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';


  constructor(public dialogRef: MatDialogRef<MensajeEditarComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any,
    public bpMedicamentoService: BpMedicamentoService,
    public snackBar: MatSnackBar) {
    this.mensaje = this.data.mensaje;
  }

  ngOnInit(): void {
  }

  actualizarCantidad() {
    this.cantidad = this.data.cantidadActual + this.nuevaCantidad;
    this.cantidad = Math.round((this.cantidad + Number.EPSILON) * 100) / 100;

    this.bpMedicamento = {
      _id: this.data._id,
      nom_medicamento: this.data.nom,
      cantidad: this.cantidad
    }

    if (this.nuevaCantidad > 0) {
      this.bpMedicamentoService.editMedicamento(this.bpMedicamento, this.data._id).subscribe(
        res => {
          this.snackBar.open('Cantidad agregada con éxito', '', {
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
            panelClass: 'exito'
          });

          setTimeout(function () {
            window.location.reload();
          }, 1000);
        },
        err => {
          console.log(err);
        }
      )
    } else {
      this.snackBar.open('La cantidad a agregar no puede ser igual o menor a 0', '', {
        horizontalPosition: this.horizontalPosition,
        verticalPosition: this.verticalPosition,
        duration: 3000,
        panelClass: 'advertencia'
      });
    }

  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}

