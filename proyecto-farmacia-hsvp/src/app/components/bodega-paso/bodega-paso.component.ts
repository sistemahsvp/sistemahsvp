import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';

//TABLA DE MEDICAMENTOS
import { MatTableDataSource } from '@angular/material/table';
import { MatDialog } from '@angular/material/dialog';
import { MsjIngresarComponent } from './msj-ingresar/msj-ingresar.component';
import { MensajeEditarComponent } from './mensaje-editar/mensaje-editar.component';
import { MensajeDespacharComponent } from './mensaje-despachar/mensaje-despachar.component';
import { MedicamentoService } from 'src/app/services/medicamento.service';
import { Medicamento } from 'src/app/models/medicamento';
import { BpMedicamentoService } from 'src/app/services/bp-medicamento.service';
import { BodegaGeneralService } from '../../services/bodega-general.service';

export interface registroTabla {
  _id: string;
  nom_medicamento: string;
  cantidad: number;
  cantidadBg: number;
}

@Component({
  selector: 'app-bodega-paso',
  templateUrl: '/bodega-paso.component.html',
  styleUrls: ['/bodega-paso.component.scss'],

})


export class BodegaPasoComponent implements OnInit {
  displayedColumns: string[] = ['_id', 'nom_medicamento', 'cantidad', 'cantidadBg', 'agregar', 'despachar'];
  dataSource = new MatTableDataSource<any>([]);
  _id: string;


  @ViewChild(MatPaginator, { static: true })
  paginator!: MatPaginator;
  @ViewChild(MatSort, { static: true })
  sort!: MatSort;

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }


  bpIngresoMedicamento: Medicamento[];

  constructor(public dialog: MatDialog,
    public medicamentoService: MedicamentoService,
    public BodegaPasoService: BpMedicamentoService,
    public BodegaGeneralService: BodegaGeneralService,) {
    this.getbodega_paso();
    this.bpIngresoMedicamento;
    this.BodegaGeneralService;
  }

  ngOnInit(): void {
    //this.getbodega_paso();
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  datosTabla = <any>[];
  getbodega_paso() {
    this.BodegaPasoService.getbodega_paso().subscribe(
      (res) => {
        res.forEach(medicamentoBP => {
          this.BodegaGeneralService.getbg_medicamento(medicamentoBP._id).subscribe(

            medicamentoBG => {

              if (medicamentoBG != null) {
                this.datosTabla.push({
                  _id: medicamentoBP._id,
                  nom_medicamento: medicamentoBP.nom_medicamento,
                  cantidad: medicamentoBP.cantidad,
                  cantidadBg: medicamentoBG.cantidad
                })
              } else {
                this.datosTabla.push({
                  _id: medicamentoBP._id,
                  nom_medicamento: medicamentoBP.nom_medicamento,
                  cantidad: medicamentoBP.cantidad,
                  cantidadBg: 0
                })
              }
              this.dataSource.data = this.datosTabla
            }
          )
        })
      },
      err => console.error(err)
    )
  }

  getMedicamentoBp(_id: string) {
    this.BodegaPasoService.getMedicametoBp(_id).subscribe(
      res => {
        this.getbodega_paso();
      },
      err => console.error(err)
    )
  }

  openDialog(): void {
    const dialogRef = this.dialog.open(MsjIngresarComponent, {
      width: '250px',
      data: { _id: this._id },
    });

    
  }

  mensajeAgregar(_id: String, cantidadActual: number, nom: String) {
    const dialogRef = this.dialog.open(MensajeEditarComponent, {
      width: '280px',
      data: { _id, cantidadActual, nom, mensaje: `Medicamento: ${nom}` },
    });
   
  }

  mensajeDespachar(_id: String, cantidadActual: number, nom: String) {
    const dialogRef = this.dialog.open(MensajeDespacharComponent, {
      width: '280px',
      data: { _id, cantidadActual, nom, mensaje: `Medicamento: ${nom}` },
    });
    
  }

}
