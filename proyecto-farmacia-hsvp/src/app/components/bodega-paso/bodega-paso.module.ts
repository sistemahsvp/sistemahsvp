import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';



import { MatButtonModule } from '@angular/material/button';
import { MatMenuModule } from '@angular/material/menu';
import { MatInputModule } from '@angular/material/input';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatTableModule } from '@angular/material/table';
import { MatSnackBarModule } from '@angular/material/snack-bar';

import { ReactiveFormsModule } from '@angular/forms';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { MsjIngresarComponent } from './msj-ingresar/msj-ingresar.component';
import { MensajeEditarComponent } from './mensaje-editar/mensaje-editar.component';

import { MatDialogModule } from '@angular/material/dialog';
import { FormsModule } from '@angular/forms';

import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { HttpClientModule } from '@angular/common/http';
import { MensajeDespacharComponent } from './mensaje-despachar/mensaje-despachar.component';



@NgModule({
  declarations: [
    MsjIngresarComponent,
    MensajeEditarComponent,
    MensajeDespacharComponent
  ],
  imports: [
    CommonModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatMenuModule,
    MatInputModule,
    MatFormFieldModule,
    MatTableModule,
    MatSnackBarModule,
    ReactiveFormsModule,
    AppRoutingModule,
    MatDialogModule,
    FormsModule,
    MatPaginatorModule,
    MatSortModule,
    HttpClientModule
  ],
  exports: [
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatMenuModule,
    MatInputModule,
    MatFormFieldModule,
    MatTableModule,
    MatSnackBarModule,
    ReactiveFormsModule,
    AppRoutingModule,
    MatDialogModule,
    FormsModule,
    MatPaginatorModule,
    MatSortModule,
    HttpClientModule
  ]
})
export class BodegaPasoModule { }
