import { Component, OnInit } from '@angular/core';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { Autorizacion } from 'src/app/models/autorizacion';
import { AutorizacionService } from 'src/app/services/autorizacion.service';
import { FormBuilder, FormGroup, NgForm } from '@angular/forms';

interface Estado {
  value: string;
  viewValue: string;
}


@Component({
  selector: 'app-editar',
  templateUrl: '/editar.component.html',
  styleUrls: ['/editar.component.scss']
})
export class EditarComponent implements OnInit {

  estados: Estado[] = [
    {value: 'Autorizado', viewValue: 'Autorizado'},
    {value: 'Denegado', viewValue: 'Denegado'},
    {value: 'Finalizado', viewValue: 'Finalizado'},
  ];

  
  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  idAutorizacion: any;
  accion = '';

  miFormulario: FormGroup;
  
  //Para actualizar
  identificador: Number;

  //Valores de una Autorización
  cedula: Number;
  nombre: String;
  cod_medicamento: String;
  nom_medicamento: String;
  dosis: String;
  diagnostico: String;
  clave_inicial: String;
  clave_continuacion: String;
  medico: String;
  especialidad: String;
  estado: String;
  cantidad_retiros: Number;

  autorizacionesModel: Autorizacion[];
  autorizacionModelo: Autorizacion;

  constructor(private route: Router, public snackBar: MatSnackBar, private aRoute: ActivatedRoute,
                private autorizacionService: AutorizacionService, private fb: FormBuilder,) {

        const idParam = 'id';
        this.idAutorizacion = this.aRoute.snapshot.params[idParam];

        this.autorizacionesModel = [];
    
     }

  //Al ingresar a la Pag, se inicia llenando los espacios correspondientes con la info que trae el Documento
  ngOnInit(): void {
    if(this.idAutorizacion != undefined) {
      this.accion = 'Editar';
      this.llenarFormularioEditar();
      
      //Para actualizar
      this.identificador = this.idAutorizacion;
    }
  }

  //Metódo para llenar el formulario con los datos del Documento respectivo, segun su _id q se le envía
  llenarFormularioEditar() {
    this.autorizacionService.getAutorizacion(this.idAutorizacion).subscribe(      
      (res) => {
        if(res != undefined){
          this.cedula = res.cedula
          this.nombre = res.nombre
          this.cod_medicamento = res.cod_medicamento
          this.nom_medicamento = res.nom_medicamento
          this.dosis = res.dosis
          this.diagnostico = res.diagnostico
          this.clave_inicial = res.clave_inicial
          this.clave_continuacion = res.clave_continuacion
          this.medico = res.medico
          this.especialidad = res.especialidad
          this.estado = res.estado
          this.cantidad_retiros = res.cantidad_retiros
        } else {
          this.snackBar.open('Datos de Autorización Incorrectos', '', {
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
            duration: 3000,
            panelClass: 'Advertencia'
          })
        }
      },
      err => console.log(err)
    );
  }

  //Metódo para Editar (Actualizar una Autorizacion), se busca por el _id (idemtificador)
  editarAutorizacion(form: any) {
    if(this.identificador != undefined) {
      this.actualizarAutorizacion();
      if(this.estado == 'Autorizado' && this.cantidad_retiros == 0){
        this.snackBar.open('No se puede autorizar con 0 cantidad de retiros', '', {
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
          duration: 5000,
          panelClass: 'Advertencia'
        })
      }else{
        this.autorizacionService.actualizarAutorizacion(this.autorizacionModelo, this.identificador).subscribe(
          res => {
            this.snackBar.open('Autorización Actualizada Exitosamente', '', {
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
              duration: 5000,
              panelClass: 'Exito'
            })
          },
          err => {
            this.snackBar.open('Error al Registrar la Autorización', '', {
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
              duration: 3000,
              panelClass: 'Error'
            })
          }
        )
        this.route.navigate(['/listaAutorizaciones']);
      } 
    }   
  }
  

  redireccionar(pag: string): void {
    if (pag === 'listaAutorizaciones') {
      this.route.navigate(['/listaAutorizaciones']);
    }
  }

  cancelarAccion(){
    this.route.navigate(['/listaAutorizaciones'])
  }

  //Creación de un Modelo para actualizar una Autorización
  actualizarAutorizacion() {
    this.autorizacionModelo = {
      cedula: this.cedula,
      nombre: this.nombre,
      cod_medicamento: this.cod_medicamento,
      nom_medicamento: this.nom_medicamento,
      dosis: this.dosis,
      diagnostico: this.diagnostico,
      clave_inicial: this.clave_inicial,
      clave_continuacion: this.clave_continuacion,
      medico: this.medico,
      especialidad: this.especialidad,
      estado: this.estado,
      cantidad_retiros: this.cantidad_retiros
    }
  }
}
