import { NgModule } from '@angular/core';
import { RouterModule, Routes} from '@angular/router';
import { ListaAutorizacionesComponent } from './lista-autorizaciones/lista-autorizaciones.component';
import { CrearComponent } from './crear/crear.component';
import { EditarComponent } from './editar/editar.component';


const routes: Routes = [
  {path: 'listaAutorizaciones', component: ListaAutorizacionesComponent},
  {path: 'editarAutorizacion/:id', component: EditarComponent},
  {path: 'crearAutorizacion', component: CrearComponent},
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AutorizacionesRoutingModule { }
