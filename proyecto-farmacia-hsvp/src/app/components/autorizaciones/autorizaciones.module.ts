import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CrearComponent } from './crear/crear.component';
import { EditarComponent } from './editar/editar.component';
import { AutorizacionesRoutingModule } from './autorizaciones-routing.module';
import { MatTableModule } from '@angular/material/table';
import { NavBarModule } from '../nav-bar/nav-bar.module';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { FormsModule } from '@angular/forms';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { MatMenuModule } from '@angular/material/menu';
import { ListaAutorizacionesComponent } from './lista-autorizaciones/lista-autorizaciones.component';
import { MensajeConfirmacionAutComponent } from './mensaje-confirmacion-aut/mensaje-confirmacion-aut.component';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatDialogModule } from '@angular/material/dialog';


@NgModule({
  declarations: [
    EditarComponent,
    CrearComponent,
    ListaAutorizacionesComponent,
    MensajeConfirmacionAutComponent
  ],
  imports: [
    CommonModule,
    AutorizacionesRoutingModule,
    MatTableModule,
    NavBarModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatFormFieldModule,
    FormsModule,
    MatInputModule,
    MatSelectModule,
    AppRoutingModule,
    MatMenuModule,
    MatPaginatorModule,
    MatDialogModule
  ],
  exports: [
    MatTableModule,
    AppRoutingModule
  ]

})
export class AutorizacionesModule {

}
