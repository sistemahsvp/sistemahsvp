import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MensajeConfirmacionAutComponent } from './mensaje-confirmacion-aut.component';

describe('MensajeConfirmacionAutComponent', () => {
  let component: MensajeConfirmacionAutComponent;
  let fixture: ComponentFixture<MensajeConfirmacionAutComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MensajeConfirmacionAutComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MensajeConfirmacionAutComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
