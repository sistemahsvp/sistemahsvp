import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';

@Component({
  selector: 'app-mensaje-confirmacion-aut',
  templateUrl: '/mensaje-confirmacion-aut.component.html',
  styleUrls: ['/mensaje-confirmacion-aut.component.scss']
})
export class MensajeConfirmacionAutComponent implements OnInit {

  mensaje: string;
  btn = 'aceptar';

  constructor(public dialogRef: MatDialogRef<MensajeConfirmacionAutComponent>,
    @Inject(MAT_DIALOG_DATA) public data: any) {
      this.mensaje = data.mensaje;
     }

  ngOnInit(): void {
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
