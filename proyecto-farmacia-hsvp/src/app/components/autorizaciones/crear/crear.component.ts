import { Component, OnInit, Inject } from '@angular/core';
import { PacienteService } from 'src/app/services/paciente.service';
import { MedicamentoService } from 'src/app/services/medicamento.service';
import { AutorizacionService } from 'src/app/services/autorizacion.service';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { Autorizacion } from 'src/app/models/autorizacion';
import { Router } from '@angular/router';

interface Estado {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-crear',
  templateUrl: '/crear.component.html',
  styleUrls: ['/crear.component.scss']
})
export class CrearComponent implements OnInit {

  estados: Estado[] = [
    {value: 'Autorizado', viewValue: 'Autorizado'},
    {value: 'Denegado', viewValue: 'Denegado'},
    {value: 'Finalizado', viewValue: 'Finalizado'},
  ];


  autorizacionesModel: Autorizacion[];
  autorizacionModelo: Autorizacion;

//atributos de Autorizacion
  cedula: number;
  nombre: string;
  cod_medicamento: string;
  nom_medicamento: string;
  dosis: string;
  diagnostico: string;
  clave_inicial: string;
  clave_continuacion = '-';
  medico: string;
  especialidad: string;
  estado: string;
  cantidad_retiros: number;

//variables para buscar la cedula del pte
  event : number;
  _id : number;
//variables para buscar el codMedicamento
  eventCod : string;
  cod : string;
//variables para buscar la clave_inicial
  eventClaveInicio: string;

//SnackBar
  horizontal : MatSnackBarHorizontalPosition = 'end';
  vertical : MatSnackBarVerticalPosition = 'top';
  

  constructor(public pacienteService: PacienteService,
              public medicamentoService: MedicamentoService,
              public autorizacionService: AutorizacionService,
              private route: Router,
              public snackbar: MatSnackBar) { 
    this.nombre = '';
    this.nom_medicamento = '';
    this.event = 0;
    this.eventCod = '';
    this.eventClaveInicio = '';

    this.autorizacionesModel = [];

    
  }


  ngOnInit(): void {
  }

  
  //Metodo para traer el Pte segun su cedula
  buscarPaciente(event: any) {
    if(event != null) {
      this._id = event;
      this.pacienteService.getPaciente(this._id).subscribe(res=>{
        if(res != undefined) {
          this.nombre = `${res.nombre} ${res.apellido1} ${res.apellido2}`
        } else {          
          this.snackbar.open('Paciente No Existe, Favor Verifique el Número de Identificación', '', {
            horizontalPosition: this.horizontal,
            verticalPosition: this.vertical,
            duration: 3000,
            panelClass: 'Advertencia'
          })
        }
      });
    }
  }

  //Metodo para traer el medicamento segun su Codigo
  buscarMedicamento(eventCod: any) {
    this.cod = eventCod;
    this.medicamentoService.getMedicamento(this.cod).subscribe(res =>{
      if(res != undefined) {
        this.nom_medicamento = `${res.nom_medicamento}`
      } else {
        this.snackbar.open('Medicamento No Existe, Favor Verifique el Código Ingresado', '', {
          horizontalPosition: this.horizontal,
          verticalPosition: this.vertical,
          duration: 3000,
          panelClass: 'Advertencia'
        })
      }
    });
  }

  //Metodo para validar si una Clave de Inicio ya existe 
  buscarClaveInicio(eventClaveInicio: any) {
    this.clave_inicial = eventClaveInicio;
    this.autorizacionService.traerAutorizacionClaveInicio(this.clave_inicial).subscribe(res =>{
      if(res != undefined) {
        this.snackbar.open('Autorización Ya Existe, Favor Verifique la Clave', '', {
          horizontalPosition: this.horizontal,
          verticalPosition: this.vertical,
          duration: 3000,
          panelClass: 'Advertencia'
        })
      } else {
        this.snackbar.open('Autorización Válida', '', {
          horizontalPosition: this.horizontal,
          verticalPosition: this.vertical,
          duration: 3000,
          panelClass: 'Admision'
        })
      }
    });
  }

  //metodo para registrar una Autorización
  registrarAutorizacion(form: any) {    
    if(form.value.cedula != undefined) {        
        this.construirAutorizacion();
        if(this.estado == 'Autorizado' && this.cantidad_retiros == 0){
        
          this.snackbar.open('No se puede autorizar con 0 cantidad de retiros', '', {
            horizontalPosition: this.horizontal,
            verticalPosition: this.vertical,
            duration: 3000,
            panelClass: 'Advertencia'
          })

        }else{

          this.autorizacionService.crearAutorizacion(this.autorizacionModelo).subscribe(
            res => {
              this.snackbar.open('Autorización Creada Exitosamente', '', {
                horizontalPosition: this.horizontal,
                verticalPosition: this.vertical,
                duration: 3000,
                panelClass: 'Exito'
              })
            },
            err => {
              this.snackbar.open('Error al Registrar la Autorización', '', {
                horizontalPosition: this.horizontal,
                verticalPosition: this.vertical,
                duration: 3000,
                panelClass: 'Error'
              })          
            }           
          )
          this.route.navigate(['/listaAutorizaciones']);
        }
    }
  }

  construirAutorizacion() {
    this.autorizacionModelo = {
      cedula: this.cedula,
      nombre: this.nombre,
      cod_medicamento: this.cod_medicamento,
      nom_medicamento: this.nom_medicamento,
      dosis: this.dosis,
      diagnostico: this.diagnostico,
      clave_inicial: this.clave_inicial,
      clave_continuacion: this.clave_continuacion,
      medico: this.medico,
      especialidad: this.especialidad,
      estado: this.estado,
      cantidad_retiros: this.cantidad_retiros
    }
  }


}
