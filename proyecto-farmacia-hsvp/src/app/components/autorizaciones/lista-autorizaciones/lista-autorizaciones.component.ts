import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator} from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { MensajeConfirmacionAutComponent } from '../mensaje-confirmacion-aut/mensaje-confirmacion-aut.component';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { AutorizacionService } from 'src/app/services/autorizacion.service';


@Component({
  selector: 'app-lista-autorizaciones',
  templateUrl: '/lista-autorizaciones.component.html',
  styleUrls: ['/lista-autorizaciones.component.scss']
})
export class ListaAutorizacionesComponent implements OnInit {

  _id: number;
  id: number;

  constructor(public dialog: MatDialog, public snackBar: MatSnackBar, private route: Router,
        private autorizacionService: AutorizacionService) {
          this.llenarTabla();
          this._id = 0;
        }

  llenarTabla(){
    this.autorizacionService.getAutorizaciones().subscribe(data => {
      this.dataSource.data = data;
    });
  }

  displayedColumns: string[] = ['cedula', 'nombre', 'nom_medicamento', 'clave_inicial',
                 'clave_continuacion', 'medico', 'estado', 'cantidad_retiros', 'editar'];
  dataSource = new MatTableDataSource<any>([]);

  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  
  ngOnInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
    this.llenarTabla();
    this.dataSource.data;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  //Metodo para Eliminar una Autorizacion segun su _id
  eliminarAutorizacion(_id: any) {
    const dialogRef = this.dialog.open(MensajeConfirmacionAutComponent, {
      width: '350px',
      data: {mensaje: `¿Está seguro de realizar esta acción?`}
    });
    
    this.id = _id;
    dialogRef.afterClosed().subscribe(result => {
      if(result === 'aceptar'){
        this.autorizacionService.eliminarAutorizacion(this.id).subscribe(res => {

          this.snackBar.open('La autorización se ha eliminado con éxito', '', {
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
            duration: 3000,
            panelClass: 'Exito'
        });
        this.llenarTabla();
      });
      this.llenarTabla();
      }
    });
  }

  redireccionar(pag: string): void{
    if(pag === 'pagPrincipal'){
      this.route.navigate(['autorizacion']);
    }
  }

  redireccionarCrearAutorizacion(): void{
    this.route.navigate(['crearAutorizacion'])
  }
  

}