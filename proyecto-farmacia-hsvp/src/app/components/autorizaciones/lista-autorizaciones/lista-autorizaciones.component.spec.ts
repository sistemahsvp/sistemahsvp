import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListaAutorizacionesComponent } from './lista-autorizaciones.component';

describe('ListaAutorizacionesComponent', () => {
  let component: ListaAutorizacionesComponent;
  let fixture: ComponentFixture<ListaAutorizacionesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListaAutorizacionesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListaAutorizacionesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
