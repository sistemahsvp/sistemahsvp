import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { Receta } from 'src/app/models/receta';
import { RecetaService } from 'src/app/services/receta.service';


@Component({
  selector: 'app-bitacora',
  templateUrl: '/bitacora.component.html',
  styleUrls: ['/bitacora.component.scss']
})
export class BitacoraComponent implements OnInit {

  displayedColumns: string[] = ['_id', 'cedula', 'nombre', 'nom_medicamento',
                                'mensaje', 'usuario', 'fecha_entrada'];
  dataSource = new MatTableDataSource<any>([]);

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;


  constructor( private route: Router,
               private RecetaService: RecetaService) {
                 this.getRecetasMsj();
               }

  ngOnInit(): void {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  //Traer Recetas con Msj
  getRecetasMsj() {
    this.RecetaService.getRecetasMsj().subscribe(
      res => {
        this.dataSource.data = res
      },
      err => console.error(err)
    )
  }

  getRecetas() {
    this.RecetaService.getRecetas().subscribe(
      res => {
        this.dataSource.data = res
      },
      err => console.error(err)
    )
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  redireccionar(pag: string): void{
    if(pag === 'pagPrincipal'){
      this.route.navigate(['administrador']);
    }
  }

  //metodo para exportar a Excel
  exportarArchivoCompleto(): void{
    this.RecetaService.exportarExcel(this.dataSource.data, 'Bitácora');
  }

  //metodo para exportar a Excel con Filtro
  exportarArchivoFiltrado(): void{
    this.RecetaService.exportarExcel(this.dataSource.filteredData, 'Bitácora Filtrada');
  }

  redireccionarAdm(pag: string): void{
    if(pag === 'pagAdministrador'){
      this.route.navigate(['/administrador']);
    }else if(pag === 'editar'){
      this.route.navigate(['/administrador']);
    }
  }

}
