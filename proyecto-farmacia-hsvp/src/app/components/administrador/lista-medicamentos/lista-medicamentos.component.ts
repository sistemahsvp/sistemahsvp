import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator} from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { MensajeConfirmacionComponent } from '../mensaje-confirmacion/mensaje-confirmacion.component'; 
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { MedicamentoService } from 'src/app/services/medicamento.service';


@Component({
  selector: 'app-lista-medicamentos',
  templateUrl: '/lista-medicamentos.component.html',
  styleUrls: ['/lista-medicamentos.component.scss']
})
export class ListaMedicamentosComponent implements OnInit {

  displayedColumns: string[] = ['_id', 'nom_medicamento','accion'];
  dataSource = new MatTableDataSource<any>([]);

  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;


  constructor(public dialog: MatDialog, 
    public snackBar: MatSnackBar, 
    private route: Router, 
    public medicamentoService: MedicamentoService) { 
      this.getMedicamentos();
    }

  ngOnInit(): void {
    this.getMedicamentos();
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  eliminarMedicamento(_id: String){
    const dialogRef = this.dialog.open(MensajeConfirmacionComponent, {
      width: '350px',
      data: {mensaje: `¿Realmente desea eliminar el medicamento: ${ _id }?`}
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result === 'aceptar'){

        this.medicamentoService.deleteMedicamento(_id).subscribe(
          res => {
            this.getMedicamentos();
          },
          err => console.log(err)
        );

        this.snackBar.open('El medicamento se ha eliminado con éxito', '', {
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
          duration: 3000,
          panelClass: ['exito']
        });
      }
    });

    this.getMedicamentos();
  }

  redireccionar(pag: string): void{
    if(pag === 'pagPrincipal'){
      this.route.navigate(['administrador']);
    }
  }

  redireccionarCrearMedicamento(){
      this.route.navigate(['crearMedicamento'])
  }

  getMedicamentos() {
    this.medicamentoService.getMedicamentos().subscribe(
      res => this.dataSource.data = res,
      err => console.log(err)
    );
  }

}
