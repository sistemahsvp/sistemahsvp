import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator} from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatDialog } from '@angular/material/dialog';
import { MensajeConfirmacionComponent } from '../mensaje-confirmacion/mensaje-confirmacion.component'; 
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-lista-usuarios',
  templateUrl: '/lista-usuarios.component.html',
  styleUrls: ['/lista-usuarios.component.scss']
})
export class ListaUsuariosComponent implements OnInit {

  displayedColumns: string[] = ['_id', 'nombre', 'apellido1',
    'apellido2', 'correo', 'telefono', 'acciones'];
  dataSource = new MatTableDataSource<any>([]);

  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(public dialog: MatDialog, public snackBar: MatSnackBar, private route: Router,
    private usuarioService: UsuarioService) {
      this.poblarTabla();
  }

  ngOnInit(): void {
    this.poblarTabla();
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  poblarTabla(){
    this.usuarioService.getUsuarios().subscribe(data =>{   
      this.dataSource.data = data;
    });
  }

  eliminarUsuario(id: string){
    const dialogRef = this.dialog.open(MensajeConfirmacionComponent, {
      width: '350px',
      data: {mensaje: `¿Está seguro de realizar esta acción?`}
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result === 'aceptar'){
        this.usuarioService.eliminarUsuario(id).subscribe(res =>{
          
          this.snackBar.open('El usuario se ha eliminado con éxito', '', {
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
            duration: 3000,
            panelClass: ['exito']
            
          });
          this.poblarTabla();
        });
        this.poblarTabla();
      }
    });
  }

  redireccionarCrearUsuario(): void{
      this.route.navigate(['UsuariosCrear'])
  }

}
