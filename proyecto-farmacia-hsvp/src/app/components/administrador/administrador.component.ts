import { Component, OnInit, ViewChild } from '@angular/core';
import { NumberValueAccessor } from '@angular/forms';
import { Chart, ChartData, registerables } from 'node_modules/chart.js';
import { BodegaGeneralService } from 'src/app/services/bodega-general.service';
import { RecetaService } from 'src/app/services/receta.service';
import { MedicamentosBG } from '../../models/bodega-general';

Chart.register(...registerables)

@Component({
  selector: 'app-administrador',
  templateUrl: '/administrador.component.html',
  styleUrls: ['/administrador.component.scss']
})
export class AdministradorComponent implements OnInit {

  // existencias: MedicamentosBG;
  existencias: Array<number> = [];
  reintegros: Array<number> = [];
  nombres: Array<string> = [];
  medicamentosJSON: any;
  datos: ChartData;
  resultadosPorMes: ChartData;
  displayedColumns = ['Nombre', 'Cantidad']

  constructor(private bgServicio: BodegaGeneralService, private recetaservice: RecetaService) {
  }

  async obtenerReintegrosPorMes() {
    this.recetaservice.getReintegrosPorMes().subscribe((res) => {
      res.forEach((value) => {
        this.reintegros.push(value);
      });
      this.resultadosPorMes = {
        labels: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        datasets: [
          {
            label: '',
            data: this.reintegros,
            backgroundColor: [
              '#2B558C',
              '#33A65B',
              '#D95829',
              '#D92534',
              '#D9B341'
            ],
          }
        ]
      };
      const medicamentosPorMes = new Chart('reintegros', {
        type: 'bar',
        data: this.resultadosPorMes,
        options: {
          responsive: true,
          plugins: {
            legend: {
              position: 'top',
              display: false
            },
            title: {
              display: true,
              text: 'Reintegros por mes'
            }
          }
        }
      })
    })
  }

  async obtenerExistencias() {
    //el numero es la cantidad maxima que pueden tener los resultados de los medicamentos
    this.bgServicio.getMenoresExistencias(5).subscribe((res) => {
      //indx es para las posiciones que va recorriendo el foreach
      res.forEach((medicamento, indx) => {
        this.existencias.push(medicamento.cantidad);
        this.nombres.push(medicamento.nom_medicamento);
      })
      this.medicamentosJSON = res;
    });
  }
  ngOnInit(): void {
    this.obtenerExistencias();
    this.obtenerReintegrosPorMes();


  }
}
