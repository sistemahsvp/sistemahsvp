import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { Usuario } from 'src/app/models/usuario';
import { UsuarioService } from 'src/app/services/usuario.service';

@Component({
  selector: 'app-crear-editar-usuario',
  templateUrl: '/crear-editar-usuario.component.html',
  styleUrls: ['/crear-editar-usuario.component.scss']
})
export class CrearEditarUsuarioComponent implements OnInit {

  roles: any[] = ['Administrador', 'Autorizador', 'Técnico de Farmacia', 'Técnico de Bodega General', 'Técnico de Bodega de Paso']
  tipos: any[] = ['Nacional', 'Extranjero']
  miFormulario: FormGroup;

  nuevoId: string;
  nuevoNombre: string
  nuevoTipo: string;
  nuevoApellido1: string;
  nuevoApellido2: string;
  nuevoCorreo: string;
  nuevoTelefono: number;
  nuevoRolTexto: string;
  nuevoClave: string;
  nuevoRolNumero: number;

  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  idUsuario: any;
  accion = 'Crear';

  constructor(private fb: FormBuilder, private route: Router,
    public snackBar: MatSnackBar, private aRoute: ActivatedRoute, private usuarioService: UsuarioService) {

    this.miFormulario = this.fb.group({
      identificacion: ['', Validators.required],//,[Validators.required,  Validators.pattern("^([1-9])*-[0-9]{4}-[0-9]{4}$")]],
      nombre: ['', [Validators.required, Validators.pattern("^[A-Z][a-zA-ZÁÉÍÓÚÑñáéíóú ]{1,60}[a-z]$")]],
      tipo: ['', [Validators.required, Validators.pattern("^[A-Z][a-zA-ZÁÉÍÓÚÑñáéíóú ]{1,60}[a-z]$")]],
      pApellido: ['', [Validators.required, Validators.pattern("^[A-Z][a-zA-ZÁÉÍÓÚÑñáéíóú ]{1,60}[a-z]$")]],
      sApellido: ['', [Validators.required, Validators.pattern("^[A-Z][a-zA-ZÁÉÍÓÚÑñáéíóú ]{1,60}[a-z]$")]],
      correo: ['', [Validators.required, Validators.pattern("^[a-zA-Z0-9-._]+@([a-zA-Z]+[.])+[a-zA-Z-]{2,3}$")]],
      tel: ['', [Validators.required, Validators.pattern("^([0-9]){8}$")]],
      rol: ['', Validators.required],
      contra: ['', [Validators.required, Validators.pattern("^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{5,}$")]]
    });
    const idParam = 'id';
    this.idUsuario = this.aRoute.snapshot.params[idParam];
  }


  ngOnInit(): void {
    if (this.idUsuario != undefined) {
      this.accion = 'Editar';
      this.usuarioService.getUsuario(this.idUsuario).subscribe(res => {
        this.revertirRoles(res.rol);
        this.miFormulario.controls.identificacion.disable();
        this.miFormulario.patchValue({
          identificacion: res._id,
          nombre: res.nombre,
          tipo: res.tipo,
          pApellido: res.apellido1,
          sApellido: res.apellido2,
          correo: res.correo,
          tel: res.telefono,
          rol: this.nuevoRolTexto,
          contra: res.clave,
        })
      })

    }

  }

  cargarValores(): Usuario {
    this.nuevoId = this.miFormulario.get('identificacion')?.value
    this.nuevoNombre = this.miFormulario.value.nombre;
    this.nuevoTipo = this.miFormulario.value.tipo;
    this.nuevoApellido1 = this.miFormulario.value.pApellido;
    this.nuevoApellido2 = this.miFormulario.value.sApellido;
    this.nuevoCorreo = this.miFormulario.value.correo;
    this.nuevoTelefono = this.miFormulario.value.tel;
    this.nuevoRolTexto = this.miFormulario.value.rol;
    this.nuevoClave = this.miFormulario.value.contra;
    this.convertirRoles(this.nuevoRolTexto);
    const nuevoUsuario = new Usuario(this.nuevoId, this.nuevoTipo, this.nuevoNombre, this.nuevoApellido1, this.nuevoApellido2, this.nuevoCorreo, this.nuevoTelefono, this.nuevoRolNumero, this.nuevoClave)
    return nuevoUsuario;
  }

  convertirRoles(nuevoRolTexto: string) {
    if (nuevoRolTexto === this.roles[0]) { this.nuevoRolNumero = 0 }
    if (nuevoRolTexto === this.roles[1]) { this.nuevoRolNumero = 1 }
    if (nuevoRolTexto === this.roles[2]) { this.nuevoRolNumero = 2 }
    if (nuevoRolTexto === this.roles[3]) { this.nuevoRolNumero = 3 }
    if (nuevoRolTexto === this.roles[4]) { this.nuevoRolNumero = 4 }
  }
  revertirRoles(nuevoRolNumero: number) {
    if (nuevoRolNumero === 0) { this.nuevoRolTexto = this.roles[0] }
    if (nuevoRolNumero === 1) { this.nuevoRolTexto = this.roles[1] }
    if (nuevoRolNumero === 2) { this.nuevoRolTexto = this.roles[2] }
    if (nuevoRolNumero === 3) { this.nuevoRolTexto = this.roles[3] }
    if (nuevoRolNumero === 4) { this.nuevoRolTexto = this.roles[4] }
  }

  crearEditarUsuario() {

    if (this.miFormulario.valid) {
      if (this.accion === 'Crear') {
        this.usuarioService.crearUsuario(this.cargarValores()).subscribe(res => {
          this.snackBar.open(`Usuario ${this.cargarValores().nombre} ${this.cargarValores().apellido1} creado correctamente`, '', {
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
            duration: 3000,
            panelClass: 'exito'
          });
        }, err => { console.log(err) })
      } else if (this.accion === 'Editar') {
        this.usuarioService.actualizarUsuario(this.cargarValores()).subscribe(res => {
          this.snackBar.open(`Usuario ${this.cargarValores().nombre} ${this.cargarValores().apellido1} modificado correctamente`, '', {
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
            duration: 3000,
            panelClass: ['exito']
          });
        }, err => { console.log(err) })

      }
      setTimeout(function () {
        window.location.reload();
      }, 1000);
      this.route.navigate(['/UsuariosLista'])
    }
  }

  cancelarAccion() {
    this.route.navigate(['/UsuariosLista'])
  }

  redireccionar(pag: string): void {
    if (pag === 'listaUsuarios') {
      this.route.navigate(['/UsuariosLista']);
    }
  }

}
