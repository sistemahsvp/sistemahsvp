import { _DisposeViewRepeaterStrategy } from '@angular/cdk/collections';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, NgForm, Validators } from '@angular/forms';
import { MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { Medicamento } from 'src/app/models/medicamento';
import { MedicamentoService } from 'src/app/services/medicamento.service';

@Component({
  selector: 'app-crear-editar-medicamentos',
  templateUrl: '/crear-editar-medicamentos.component.html',
  styleUrls: ['/crear-editar-medicamentos.component.scss']
})
export class CrearEditarMedicamentosComponent implements OnInit {

  miFormulario: FormGroup; 

  horizontalPosition: MatSnackBarHorizontalPosition = 'end';
  verticalPosition: MatSnackBarVerticalPosition = 'top';

  idMedicamento: any;
  accion = 'Agregar';

  constructor(private fb: FormBuilder, private route: Router, 
    public snackBar: MatSnackBar, private aRoute: ActivatedRoute,
    public medicamentoService: MedicamentoService) {

      this.miFormulario = this.fb.group({
        _id: ['', [Validators.required, Validators.pattern("^([1-9])*-[0-9]{2}-[0-9]{2}-[0-9]{4}$")]],
        nom_medicamento: ['', [Validators.required, Validators.pattern("^[A-Z][a-zA-ZÁÉÍÓÚÑñáéíóú0-9 ]{1,60}[a-z]$")]]
      });
      const idParam = 'id';
      this.idMedicamento = this.aRoute.snapshot.params[idParam];
     }

  ngOnInit(): void {
    if(this.idMedicamento != undefined){
      this.accion = 'Editar';
      this.llenarFormularioEditar();
    }
  }

  crearEditarMedicamento(form: any){
    if(this.accion === 'Agregar'){

      this.medicamentoService.getMedicamento(form.value._id).subscribe(
        (res) =>{
          if(res != null){
            this.snackBar.open('Código del medicamento inválido o ya existe', '', {
              horizontalPosition: this.horizontalPosition,
              verticalPosition: this.verticalPosition,
              duration: 3000,
              panelClass: 'advertencia'
            });
          }else{
            this.agregarMedicamento();
          }         
        }, 
        err => {
          console.log(err);
        }
      );
      

    }else if(this.accion === 'Editar'){

      this.editarMedicamento(form.value);
           
    }
      
  }

  agregarMedicamento() {
    this.medicamentoService.createMedicamentos(this.miFormulario.value).subscribe(
      res => {
            this.snackBar.open('Medicamento agregado de manera correcta', '', {
            horizontalPosition: this.horizontalPosition,
            verticalPosition: this.verticalPosition,
            duration: 3000,
            panelClass: 'exito'
            });
          },
    err => console.log(err)
    );

    
    this.route.navigate(['/MedicamentosLista']); 
  }

  editarMedicamento(medicamento: Medicamento) {

      this.medicamentoService.updateMedicamento(medicamento, this.idMedicamento).subscribe(
        res => {
          this.snackBar.open('Medicamento modificado con éxito', '', {
          horizontalPosition: this.horizontalPosition,
          verticalPosition: this.verticalPosition,
          duration: 3000,
          panelClass: 'exito'
          });
        },
        err => console.log(err)
      );
      
      
    this.route.navigate(['/MedicamentosLista']); 
  }

  llenarFormularioEditar() {

    this.medicamentoService.getMedicamento(this.idMedicamento).subscribe(
      (res: Medicamento) =>{
        this.medicamentoService.medicamento = res
        this.miFormulario.controls._id.disable();
        this.miFormulario.patchValue({
          _id: this.medicamentoService.medicamento._id,
          nom_medicamento: this.medicamentoService.medicamento.nom_medicamento
        })
      }, 
      err => console.log(err)
    );
    
  }

  redireccionar(pag: string): void{
    if(pag === 'listaMedicamentos'){
      this.route.navigate(['/MedicamentosLista']);
    }
  }

}
