import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CrearEditarMedicamentosComponent } from './crear-editar-medicamentos/crear-editar-medicamentos.component';
import { CrearEditarUsuarioComponent } from './crear-editar-usuario/crear-editar-usuario.component';
import { ListaMedicamentosComponent } from './lista-medicamentos/lista-medicamentos.component';
import { ListaUsuariosComponent } from './lista-usuarios/lista-usuarios.component';
import { BitacoraComponent } from './bitacora/bitacora.component';

const routes: Routes = [
  { path: 'UsuariosLista', component: ListaUsuariosComponent },
  { path: 'editarUsuario/:id', component: CrearEditarUsuarioComponent },
  { path: 'crearMedicamento', component: CrearEditarMedicamentosComponent },
  { path: 'UsuariosCrear', component: CrearEditarUsuarioComponent },
  { path: 'MedicamentosLista', component: ListaMedicamentosComponent },
  { path: 'editarMedicamento/:id', component: CrearEditarMedicamentosComponent },
  { path: 'bitacora', component: BitacoraComponent }
]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AdministradorRoutingModule { }
