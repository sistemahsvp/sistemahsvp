import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

//Angular Material Imports
import { MatTableModule } from '@angular/material/table';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatSortModule } from '@angular/material/sort';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatCardModule } from '@angular/material/card';
import { MatSelectModule } from '@angular/material/select';
import { ReactiveFormsModule } from '@angular/forms';
import { AdministradorRoutingModule } from './administrador-routing.module';
import { CrearEditarMedicamentosComponent } from './crear-editar-medicamentos/crear-editar-medicamentos.component';
import { CrearEditarUsuarioComponent } from './crear-editar-usuario/crear-editar-usuario.component';
import { MensajeConfirmacionComponent } from './mensaje-confirmacion/mensaje-confirmacion.component';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { ListaUsuariosComponent } from './lista-usuarios/lista-usuarios.component';
import { ListaMedicamentosComponent } from './lista-medicamentos/lista-medicamentos.component';
import { AppRoutingModule } from 'src/app/app-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { BitacoraComponent } from './bitacora/bitacora.component';



@NgModule({
  declarations: [
    CrearEditarMedicamentosComponent,
    CrearEditarUsuarioComponent,
    MensajeConfirmacionComponent,
    ListaUsuariosComponent,
    ListaMedicamentosComponent,
    BitacoraComponent
  ],
  imports: [
    CommonModule,
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    MatPaginatorModule,
    MatSortModule,
    MatButtonModule,
    MatDialogModule,
    MatSnackBarModule,
    MatCardModule,
    MatSelectModule,
    ReactiveFormsModule,
    AdministradorRoutingModule,
    MatToolbarModule,
    MatIconModule,
    MatMenuModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule

  ],
  exports: [
    MatTableModule,
    MatFormFieldModule,
    MatInputModule,
    MatPaginatorModule,
    MatSortModule,
    MatButtonModule,
    MatDialogModule,
    MatSnackBarModule,
    MatCardModule,
    MatSelectModule,
    ReactiveFormsModule,
    AdministradorRoutingModule,
    MatToolbarModule,
    MatIconModule,
    MatMenuModule,
    AppRoutingModule
  ]
})
export class AdministradorModule { }
