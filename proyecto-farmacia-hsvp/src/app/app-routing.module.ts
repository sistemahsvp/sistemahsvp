import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AdministradorComponent } from './components/administrador/administrador.component';
import { BitacoraComponent } from './components/administrador/bitacora/bitacora.component';
import { CrearEditarMedicamentosComponent } from './components/administrador/crear-editar-medicamentos/crear-editar-medicamentos.component';
import { CrearEditarUsuarioComponent } from './components/administrador/crear-editar-usuario/crear-editar-usuario.component';
import { ListaMedicamentosComponent } from './components/administrador/lista-medicamentos/lista-medicamentos.component';
import { ListaUsuariosComponent } from './components/administrador/lista-usuarios/lista-usuarios.component';
import { AutorizacionesComponent } from './components/autorizaciones/autorizaciones.component';
import { CrearComponent } from './components/autorizaciones/crear/crear.component';
import { EditarComponent } from './components/autorizaciones/editar/editar.component';
import { BodegaGeneralComponent } from './components/bodega-general/bodega-general.component';
import { BodegaPasoComponent } from './components/bodega-paso/bodega-paso.component';
import { LoginComponent } from './components/login/login.component';
import { ConsultarComponent } from './components/tecnicos/consultar/consultar.component';
import { TecnicosComponent } from './components/tecnicos/tecnicos.component';
import { AdminGuard } from './guard/admin.guard';
import { AutorizacionesGuard } from './guard/autorizaciones.guard';
import { BodegaGeneralGuard } from './guard/bodega-general.guard';
import { BodegaPasoGuard } from './guard/bodega-paso.guard';
import { LoginGuard } from './guard/login.guard';
import { TecnicosGuard } from './guard/tecnicos.guard';
import { ListaAutorizacionesComponent } from './components/autorizaciones/lista-autorizaciones/lista-autorizaciones.component';

const routes: Routes = [

  //RUTAS LOGIN
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent, canActivate: [LoginGuard] },

  //RUTAS AUTORIZACIONES
  { path: 'listaAutorizaciones', component: ListaAutorizacionesComponent, canActivate: [AutorizacionesGuard] },
  { path: 'editarAutorizacion/:id', component: EditarComponent, canActivate: [AutorizacionesGuard] },


  { path: 'autorizaciones', component: AutorizacionesComponent, canActivate: [AutorizacionesGuard] },
  { path: 'crearAutorizacion', component: CrearComponent, canActivate: [AutorizacionesGuard] },

  //RUTAS ADMINISTRADOR
  { path: 'administrador', component: AdministradorComponent, canActivate: [AdminGuard] },
  { path: 'UsuariosLista', component: ListaUsuariosComponent, canActivate: [AdminGuard] },
  { path: 'UsuariosCrear', component: CrearEditarUsuarioComponent, canActivate: [AdminGuard] },
  { path: 'editarUsuario/:id', component: CrearEditarUsuarioComponent, canActivate: [AdminGuard] },
  { path: 'MedicamentosLista', component: ListaMedicamentosComponent, canActivate: [AdminGuard] },
  { path: 'crearMedicamento', component: CrearEditarMedicamentosComponent, canActivate: [AdminGuard] },
  { path: 'editarMedicamento/:id', component: CrearEditarMedicamentosComponent, canActivate: [AdminGuard] },
  { path: 'bitacora', component: BitacoraComponent, canActivate: [AdminGuard] },

  //RUTAS BODEGA GENERAL
  { path: 'bodegaGeneral', component: BodegaGeneralComponent, canActivate: [BodegaGeneralGuard] },

  //RUTAS BODEGA DE PASO
  { path: 'bodegaPaso', component: BodegaPasoComponent, canActivate: [BodegaPasoGuard] },

  //RUTAS TECNICOS
  { path: 'tecnicos', component: TecnicosComponent, canActivate: [TecnicosGuard] },
  { path: 'consultarPerfil', component: ConsultarComponent, canActivate: [TecnicosGuard] },

  //RUTA Default
  { path: '**', redirectTo: 'login', pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
