import { TestBed } from '@angular/core/testing';

import { ConsultarMedicamentoService } from './consultar-medicamento.service';

describe('ConsultarMedicamentoService', () => {
  let service: ConsultarMedicamentoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ConsultarMedicamentoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
