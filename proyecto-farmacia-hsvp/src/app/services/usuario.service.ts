import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Usuario } from '../models/usuario';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  usuarioModelo: Usuario;
  readonly URL_API = environment.usuario;

  constructor(private http:HttpClient) { 
    this.usuarioModelo = new Usuario();
  }

  getUsuario(id:string) : Observable<Usuario>{
    return this.http.get<Usuario>(this.URL_API+id)
  }
  getUsuarioLogin(id:string, clave: string) : Observable<Usuario>{
    return this.http.get<Usuario>(`${this.URL_API}${id}/usuario/${clave}`)
  }

  getUsuarios(): Observable<Usuario[]>{
    return this.http.get<Usuario[]>(this.URL_API)
  }

  crearUsuario(usuario: Usuario){
    return this.http.post(this.URL_API, usuario)
  }

  actualizarUsuario(usuario: Usuario){
    
    return this.http.put(this.URL_API+usuario._id, usuario);
  }

  eliminarUsuario(id: String){
    return this.http.delete(this.URL_API + id)
  }
}
