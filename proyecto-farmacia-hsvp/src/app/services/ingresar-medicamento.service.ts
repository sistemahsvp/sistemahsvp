import { Injectable } from '@angular/core';
import { IngresarMedicamento } from '../models/ingresarMedicamento';
import { HttpClient } from '@angular/common/http';
import { MedicamentosBG } from '../models/bodega-general';
import { Medicamento } from '../models/medicamento';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class IngresarMedicamentoService {

  URL_API = environment.bp_Medicamentos;

  listaIngresarMedicamento: IngresarMedicamento[];
  
  //Se utiliza bpIngresoMedicametno para traer el nombre del Medicamento segun el codigo
  bpIngresoMedicamento: Medicamento[];

  constructor(private http: HttpClient) {
    this.listaIngresarMedicamento = [];
    this.bpIngresoMedicamento = [];
   }

  getIngresarMedicamento(){
    return this.listaIngresarMedicamento.slice();
  }

  //metodo para traer el modelo para crear un medicamento
  getCrearMedicamento(_id: string): Observable<Medicamento> {
    return this.http.get<Medicamento>(`${this.URL_API}/${_id}`);
  }
}
