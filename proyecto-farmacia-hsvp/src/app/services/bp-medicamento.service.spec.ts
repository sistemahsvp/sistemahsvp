import { TestBed } from '@angular/core/testing';

import { BpMedicamentoService } from './bp-medicamento.service';

describe('BpMedicamentoService', () => {
  let service: BpMedicamentoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BpMedicamentoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
