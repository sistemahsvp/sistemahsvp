import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Medicamento } from '../models/medicamento';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MedicamentoService {

  medicamento: Medicamento = {
    _id: '',
    nom_medicamento: ''
  };

  medicamentos: Medicamento[];

  URL_API = environment.medicamento;

  constructor(private http: HttpClient) { }

  getMedicamentos() {
    return this.http.get<Medicamento[]>(this.URL_API);
  }

  getMedicamento(_id: String): Observable<Medicamento> {
    return this.http.get<Medicamento>(`${this.URL_API}/${_id}`);
  }

  createMedicamentos(medicamento: Medicamento) {
    return this.http.post<Medicamento[]>(this.URL_API, medicamento);
  }

  deleteMedicamento(_id: String) {
    return this.http.delete<Medicamento[]>(`${this.URL_API}/${_id}`);
  }

  updateMedicamento(medicamento: Medicamento, _id: String){
    return this.http.put<Medicamento[]>(`${this.URL_API}/${_id}`, medicamento);
  }


}
