import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ConsultarMedicamento } from '../models/consultarMedicamento';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ConsultarMedicamentoService {

  URL_API = environment.bp_medicamentos;

  constructor(private http: HttpClient) { }

  getConsultarMedicamento(_id: String): Observable<ConsultarMedicamento>{
    return this.http.get<ConsultarMedicamento>(`${this.URL_API}/${_id}`);
  }
}
