import { TestBed } from '@angular/core/testing';

import { IndicacionService } from './indicacion.service';

describe('IndicacionService', () => {
  let service: IndicacionService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(IndicacionService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
