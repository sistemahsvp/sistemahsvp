import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Indicacion } from '../models/indicacion';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class IndicacionService {

  URL_API = environment.indicacion;

  selectedIndicacion: Indicacion = {
    _id: 0,
    nom_indicacion: '',
  };

  indicacionModelo: Indicacion[];

  constructor(private http: HttpClient) { 
    this.indicacionModelo = [];
  }

  //Todas las Indicaciones
  getIndicaciones(){
    return this.http.get<Indicacion[]>(this.URL_API);    
  }

  //Una Indicacion
  getIndicacion(_id: Number):Observable<Indicacion>{
    return this.http.get<Indicacion>(`${this.URL_API}/${_id}`);
  }
}
