import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Autorizacion } from '../models/autorizacion';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AutorizacionService {

  autorizaciones: Autorizacion[];

  URL_API = environment.autorizacion;

  constructor(private http: HttpClient) { }

  //Traer todas las autorizaciones
  getAutorizaciones() {
    return this.http.get<Autorizacion[]>(this.URL_API);
  }

  //Traer una autorizacion
  getAutorizacion(_id: Number): Observable<Autorizacion> {
    return this.http.get<Autorizacion>(`${this.URL_API}/${_id}`);
  }

  //Crear autorizacion
  crearAutorizacion(autorizacion: Autorizacion) {
    return this.http.post<Autorizacion[]>(this.URL_API, autorizacion)
  }

  //Actualizar autorizacion
  actualizarAutorizacion(autorizacion: Autorizacion, _id: Number) {
    return this.http.put<Autorizacion[]>(`${this.URL_API}/${_id}`, autorizacion);
  }

  //Eliminar autorizacion
  eliminarAutorizacion(_id: Number) {
    return this.http.delete<Autorizacion[]>(`${this.URL_API}/${_id}`)
  }

  //traer una Autorizacion segun la clave_inicial
  traerAutorizacionClaveInicio(clave_inicial: String) {
    return this.http.get<Autorizacion[]>(`${this.URL_API}/operacion/claveInicio/${clave_inicial}`);
  }


  getAutorizacionCedMed(cedula: Number, codMed: String): Observable<Autorizacion[]> {
    return this.http.get<Autorizacion[]>(`${this.URL_API}/${cedula}/autorizacion/${codMed}`);
  }

  actualizarAutorizacionPorClaveInicial(autorizacion: Autorizacion, clave_inicial: String) {
    return this.http.put<Autorizacion[]>(`${this.URL_API}/claveInicio/${clave_inicial}`, autorizacion);
  }
}
