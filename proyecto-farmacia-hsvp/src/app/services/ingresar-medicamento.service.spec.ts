import { TestBed } from '@angular/core/testing';

import { IngresarMedicamentoService } from './ingresar-medicamento.service';

describe('IngresarMedicamentoService', () => {
  let service: IngresarMedicamentoService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(IngresarMedicamentoService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
