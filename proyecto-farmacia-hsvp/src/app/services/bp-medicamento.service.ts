import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { bpMedicamentoModel } from '../models/bpMedicamento.model';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class BpMedicamentoService {


  URL_API = environment.bp_Medicamentos;

  selectedBpMedicamento: bpMedicamentoModel = {
    _id: '',
    nom_medicamento: '',
    cantidad: 0,
  };

  bpMedicamentos: bpMedicamentoModel[];

  constructor(private http: HttpClient) {
    this.bpMedicamentos = []
   }

   crearbpMedicamento(bpMedicamento: bpMedicamentoModel) {
     return this.http.post(this.URL_API, bpMedicamento);
   }

   getbodega_paso(){
    return this.http.get<bpMedicamentoModel[]>(this.URL_API);
  }

  getMedicametoBp(_id: string):Observable<bpMedicamentoModel>{
    return this.http.get<bpMedicamentoModel>(`${this.URL_API}/${_id}`);
  }

  editMedicamento(bpMedicamento: bpMedicamentoModel, _id: String){
    return this.http.put<bpMedicamentoModel[]>(`${this.URL_API}/${_id}`, bpMedicamento);
   }
}
