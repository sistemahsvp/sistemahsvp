import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Receta } from '../models/receta';
import * as FileSaver from 'file-saver';
import * as XLSX from 'xlsx';
import { environment } from 'src/environments/environment';

const EXCEL_TYPE = 'aplication/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset = UTF-8';
const EXCEL_EXT = '.xlsx';

@Injectable({
  providedIn: 'root'
})
export class RecetaService {

  URL_API = environment.receta;

  recetaModelo: Receta[];


  constructor(private http: HttpClient) {
    this.recetaModelo = [];

  }

  //Todas las Recetas
  getRecetas() {
    return this.http.get<Receta[]>(this.URL_API);
  }

  //Traer Recetas con Msj
  getRecetasMsj() {
    return this.http.get<Receta[]>(`${this.URL_API}/operacion/mensaje`);
  }

  //Una Receta
  getReceta(_id: string) {
    return this.http.get<Receta>(`${this.URL_API}/${_id}`);
  }

  getRecetasPorPaciente(cedula: Number): Observable<Receta[]> {
    return this.http.get<Receta[]>(`${this.URL_API}/paciente/${cedula}`)
  }

  //Receta por Cedula y Codigo de Medicamento
  getRecetasPorCedulaMedicamento(cedula: Number, cod_medicamento: String): Observable<Receta[]> {
    return this.http.get<Receta[]>(`${this.URL_API}/recetasCedulaMedicamento/${cedula}/receta/recetasCedulaMedicamento/${cod_medicamento}`)
  }

  //Receta por Num Receta y Cedula
  getNumRecetaCedula(_id: String, cedula: Number): Observable<Receta[]> {
    return this.http.get<Receta[]>(`${this.URL_API}/recetasNumRecetaCedula/${_id}/receta/recetasNumRecetaCedula/${cedula}`)
  }

  //Crear Receta
  crearReceta(receta: Receta) {
    return this.http.post<Receta[]>(this.URL_API, receta)
  }

  //Actualizar Receta
  actualizarReceta(receta: Receta, _id: String) {
    return this.http.put<Receta[]>(`${this.URL_API}/${_id}`, receta);
  }


  //Exportar a Excel
  exportarExcel(json: any[], excelFileName: string): void {
    const worksheet: XLSX.WorkSheet = XLSX.utils.json_to_sheet(json);
    const workbook: XLSX.WorkBook = {
      Sheets: { 'data': worksheet },
      SheetNames: ['data']
    };
    const excelBuffer: any = XLSX.write(workbook, { bookType: 'xlsx', type: 'array' });
    //Llamar al metodo q es el q va a guardar el fichero
    this.guardarExcel(excelBuffer, excelFileName);
  }

  private guardarExcel(buffer: any, fileName: string): void {
    const data: Blob = new Blob([buffer], { type: EXCEL_TYPE });
    FileSaver.saveAs(data, fileName = 'Bitácora' + EXCEL_EXT);
  }


  getReintegrosPorMes(): Observable<number[]> {
    return this.http.get<number[]>(`${this.URL_API}/reintegrosPorMes`);
  }

}
