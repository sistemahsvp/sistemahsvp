import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Paciente } from '../models/paciente';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class PacienteService {

  URL_API = environment.paciente;

  
  selectedPaciente: Paciente = {
    _id: 0,
    nombre: '',
    apellido1: '',
    apellido2: '',
  };

   pacienteModelo: Paciente[];

  constructor(private http: HttpClient) { 
    this.pacienteModelo = [];
  }

  //Todos los Pacientes
  getPacientes(){
    return this.http.get<Paciente[]>(this.URL_API);   
  }

  //Un Paciente
  getPaciente(_id: number): Observable<Paciente>{
    return this.http.get<Paciente>(`${this.URL_API}/${_id}`);
  }
}
