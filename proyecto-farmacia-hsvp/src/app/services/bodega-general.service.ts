import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MedicamentosBG } from '../models/bodega-general'
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment'; 

@Injectable({
  providedIn: 'root'
})
export class BodegaGeneralService {

  URL_API = environment.bg_medicamentos;

  selectedBpMedicamento: MedicamentosBG = {
    _id: '',
    nom_medicamento: '',
    cantidad: 0,
  };

  medicamentosBG: MedicamentosBG[];

  constructor(private http: HttpClient){
    this.medicamentosBG = [];
  }

  getbodega_general(){
    return this.http.get<MedicamentosBG[]>(this.URL_API);    
  }

  //me trae un medicamento segun el id que le envio
  getMedicamento(_id: string) {
    return this.http.get(`${this.URL_API}/${_id}`);
  }

  crearMedicamentoBG(medicamentoBG: MedicamentosBG) {
    return this.http.post<MedicamentosBG[]>(this.URL_API, medicamentoBG)
  }

  getbg_medicamento(_id: string):Observable<MedicamentosBG> {
    return this.http.get<MedicamentosBG>(`${this.URL_API}/${_id}`);
  }
  
  updateCantidadBG(medicamentosBG: MedicamentosBG, _id: String){
    return this.http.put<MedicamentosBG[]>(`${this.URL_API}/${_id}`, medicamentosBG);
  }

  getMenoresExistencias(number: number){
    return this.http.get<MedicamentosBG[]>(`${this.URL_API}/consultas/${number}`);
  }
}
