import { TestBed } from '@angular/core/testing';

import { BodegaGeneralService } from './bodega-general.service';

describe('BodegaGeneralService', () => {
  let service: BodegaGeneralService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(BodegaGeneralService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
