import { Injectable } from '@angular/core';
import { CanActivate, Router} from '@angular/router';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class BodegaPasoGuard implements CanActivate {
 
  constructor(private authService: AuthService, private router: Router){
    
  }

  canActivate() {
    if(this.authService.IsLoggedIn() && sessionStorage.getItem('rol') === '4'){
      return true;
    }
    sessionStorage.removeItem('usuario');
    sessionStorage.removeItem('rol');
    this.router.navigate(['/login']);
    return false;
  } 
}
