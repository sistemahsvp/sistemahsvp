import { Injectable } from '@angular/core';
import { CanActivate, Router} from '@angular/router';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate {

  constructor(private router: Router){}

  canActivate() {
    sessionStorage.removeItem('usuario');
    sessionStorage.removeItem('rol');
    return true;
  } 
}
