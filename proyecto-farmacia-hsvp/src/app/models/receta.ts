export class Receta {
  _id: String;
  cedula: Number;
  nombre: String;
  cod_medicamento: String;
  nom_medicamento: String;
  cod_indicacion: Number;
  nom_indicacion: String;
  estado: String;
  cantidad: Number;
  usuario: String;
  fecha_entrada: Date;
  fecha_retiro: Date;
  fecha_reintegro: Date;
  responsable: String;
  mensaje: String;
}