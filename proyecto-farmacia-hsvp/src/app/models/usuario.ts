export class Usuario{
    
    constructor(_id='', tipo='', nombre='', apellido1='',apellido2='',correo='',telefono=0,rol=0,clave=''){
        this._id = _id;
        this.tipo = tipo;
        this.nombre = nombre;
        this.apellido1 = apellido1;
        this.apellido2 = apellido2;
        this.correo = correo;
        this.telefono = telefono;
        this.rol = rol;
        this.clave = clave;

    }
    
    _id:string;
    tipo: string;
    nombre: string;
    apellido1: string;
    apellido2: string;
    correo: string;
    telefono: number;
    rol: number;
    clave: string;
}