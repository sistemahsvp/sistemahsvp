export class Autorizacion {

  constructor(cedula=0, nombre='', cod_medicamento='', nom_medicamento='', dosis='', diagnostico='',
              clave_inicial='', clave_continuacion='', medico='', especialidad='', estado='', cantidad_retiros=0) {

      this.cedula = cedula;
      this.nombre = nombre;
      this.cod_medicamento = cod_medicamento;
      this.nom_medicamento = nom_medicamento;
      this.dosis = dosis;
      this.diagnostico = diagnostico;
      this.clave_inicial = clave_inicial;
      this.clave_continuacion = clave_continuacion;
      this.medico = medico;
      this.especialidad = especialidad;
      this.estado = estado;
      this.cantidad_retiros = cantidad_retiros;
  }

  cedula: Number;
  nombre: String;
  cod_medicamento: String;
  nom_medicamento: String;
  dosis: String;
  diagnostico: String
  clave_inicial: String;
  clave_continuacion: String;
  medico: String;
  especialidad: String;
  estado: String; 
  cantidad_retiros: Number;
}