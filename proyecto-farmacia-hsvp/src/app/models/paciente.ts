export class Paciente {

    constructor(_id=0, nombre = '', apellido1='', apellido2=''){
      this._id=_id;
      this.nombre = nombre;
      this.apellido1 = apellido1;
      this.apellido2 = apellido2
    }
    _id: number;
    nombre: string; //o Nombre Completo
    apellido1:  string;
    apellido2: string;
  }