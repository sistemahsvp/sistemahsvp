// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  usuario: 'http://localhost:3000/usuario/',
  receta: 'http://localhost:3000/receta',
  paciente: 'http://localhost:3000/paciente',
  medicamento: 'http://localhost:3000/api/medicamentos',
  bp_ingreso_medicamento: 'http://localhost:3000/api/bp_ingreso_medicamento',
  indicacion: 'http://localhost:3000/indicacion',
  bg_medicamentos: 'http://localhost:3000/api/bg_medicamentos',
  bp_Medicamentos: 'http://localhost:3000/api/bp_Medicamentos',
  autorizacion: 'http://localhost:3000/autorizacion'
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
