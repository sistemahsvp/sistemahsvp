export const environment = {
  production: true,
  usuario: 'http://52.188.81.25:3000/usuario/',
  receta: 'http://52.188.81.25:3000/receta',
  paciente: 'http://52.188.81.25:3000/paciente',
  medicamento: 'http://52.188.81.25:3000/api/medicamentos',
  bp_ingreso_medicamento: 'http://52.188.81.25:3000/api/bp_ingreso_medicamento',
  indicacion: 'http://52.188.81.25:3000/indicacion',
  bg_medicamentos: 'http://52.188.81.25:3000/api/bg_medicamentos',
  bp_Medicamentos: 'http://52.188.81.25:3000/api/bp_Medicamentos',
  autorizacion: 'http://52.188.81.25:3000/autorizacion'
};
